# Main target
.PHONY: all
all: lib doc

# Utilities, like .cat, .echo
include make/Utils.mk

# Library building rules
include make/Core.mk

# Documentation building rules
include make/Doc.mk

#### Phony rules ####

## Tests

.PHONY: test
test:
	ocamlbuild -tag annot -use-ocamlfind -package oUnit -package camllib -I core test/run_tests.native --

.PHONY: debug
debug:
	ocamlbuild -tag annot -tag debug -use-ocamlfind -package oUnit -package camllib -I core test/run_tests.native --

## Bench

.PHONY: bench
bench: bench/hgraph_bench.csv

.PHONY: bench/hgraph_bench.csv
bench/hgraph_bench.csv:
	ocamlbuild -tag annot -use-ocamlfind -package camllib -I core bench/hgraph_bench.native -- | perl -pe 's#\.#,#' > $@

## Demo

.PHONY: demo
demo:
	ocamlbuild -tag annot -tag debug -use-ocamlfind -package camllib -I core demo/unstructured_history.native --

## Clean

.PHONY: clean
clean:
	ocamlbuild -clean
	rm -rf _build doc fig/*.aux fig/*.log fig/*.pdf fig/*.html fig/*.png bench/*.csv fig/*.dot demo.dot
	find . \( -name '*~' -o -name '*.backup' -o -type l \) -delete

## Demo

.PHONY: loop_checker
loop_checker:
	ocamlbuild -tag annot -tag debug -use-ocamlfind -package camllib -I core loop_checker/checker_demo.native --


Graph Based Static Analysis (GBSA)
==================================

OCaml lLibrary for performing abstract-interpretation based analysis on programs
represented by graphs.

Installation
************

.. code::

   sudo apt install doc2tex texlive-latex-extra  # Big. For doc generation only.
   sudo sed -i '/disable ghostscript format types/,+6d' /etc/ImageMagick-6/policy.xml
   opam install camllib
   

License
*******

GPLv3

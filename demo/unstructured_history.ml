(** Demonstration of the analysis tool. *)

module type DATA = sig
  type t
  val print : Format.formatter -> t -> unit 
end

module HistDom = functor (N:DATA) -> functor (L:DATA) -> struct
  type node = N.t
  type label = L.t
  type seq = label list
  type nseq = label Sette.t
  type abs = (seq,nseq) Mappe.t
  type context = unit
  let bot = Mappe.empty
  let join = Mappe.merge Sette.union
  let le a b = Mappe.equal Sette.equal (join a b) b
  let diff a _ = a
  let narrow = false
  let widen _ _ = `Stable join
  let append x e =
    Mappe.fold (fun s n acc ->
      let s, n =
	if Sette.mem e n then s,n
	else match List.partition (fun e' -> e' = e) s with
	| [], _ -> e::s, n
	| _,rem -> rem, Sette.add e n in
      let bind = Mappe.add s n Mappe.empty in
      join acc bind) x Mappe.empty
  let lift e =
    Mappe.add [e] Sette.empty Mappe.empty
  let interpret _ a e _ =
    if Array.length a = 0
    then lift e
    else Array.fold_left (fun acc (_,x) -> join acc (append x e)) Mappe.empty a
  let print_node = N.print
  let print_label = L.print
  let print_abs =
    let print_seq fmt l = Print.list ~first:"" ~sep:"." ~last:"" print_label fmt (List.rev l) in
    let print_nseq = Sette.print ~first:"{" ~last:"}" print_label in
    Mappe.print ~first:"[@[" ~sep:"@\n" ~last:"@]]@\n" ~sepbind:" + "
      print_seq print_nseq
end

module Node = struct
  type t = int
  let print = Format.pp_print_int
end
  
module Label = struct
  type t = char
  let print = Format.pp_print_char
end

module A = Analysis.Make(HistDom(Node)(Label))

let demo () =
  let graph = Hgraph.of_tuple_list [
    [],'$',1;
    [1],'A',2;
    [2],'B',3;
    [3],'C',4;
    [4],'D',5;
    [1],'E',3;
    [3],'F',5;
    [4],'G',2;
  ] in
  let pp_n, pp_l = Format.pp_print_int, Format.pp_print_char in
  let dot = "demo.dot" in
  Format.printf "Graph (see %s):@\n%! @[%a@]@\n" dot (Hgraph.print pp_n pp_l) graph;
  Hgraph.dump_dot pp_n pp_l graph (open_out dot);

  let wto =
    let open Wto in
    of_list
    [Node 1;
     Component (Component.of_list
		  [Node 2;
		   Node 3;
		   Node 4]);
     Node 5] in
  
  let wg = Wgraph.make graph wto in

  Format.printf "Ready for the analysis@\n%!";

  let res = A.analysis wg () in
  ignore res

let _ = demo ()

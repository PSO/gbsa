
let doc_ex_hedges = List.map Hgraph.Hedge.of_tuple
  [[]   ,'i',1;
   [1]  ,'f',2;
   [1]  ,'g',3;
   [2;3],'h',4;
   [4]  ,'b',3]
let doc_ex = Hgraph.of_hedge_list doc_ex_hedges

let make_html_formatter oc =
  let print = output_string oc in
  let print_char = output_char oc in
  Format.make_formatter
    (fun str pos len ->
      for i = pos to pos + len - 1 do
	match String.get str i with
	| '<' -> print "&lt;"
	| '>' -> print "&gt;"
	| '&' -> print "&amp;"
	| '\n' -> print "<br/>\n"
	| ' ' -> print "&nbsp;"
	| c -> print_char c
      done)
    (fun () -> flush oc)

let main () =
  let oc = stdout in
  let hf = make_html_formatter oc in
  output_string oc "<html><body>\n";
  Format.fprintf hf "%a%!" (Hgraph.print Format.pp_print_int Format.pp_print_char) doc_ex;
  output_string oc "</html></body>\n"

let _ = main ()


let doc_ex_hedges = List.map Hgraph.Hedge.of_tuple
  [[]   ,'i',1;
   [1]  ,'f',2;
   [1]  ,'g',3;
   [2;3],'h',4;
   [4]  ,'b',3]
let doc_ex = Hgraph.of_hedge_list doc_ex_hedges

let main () =
  Hgraph.dump_dot
    Format.pp_print_int
    Format.pp_print_char
    doc_ex
    stdout

let _ = main ()

(** Integer interval abstract domain. *)

(* Inspired from orange : O_range/conditionAnalysis.ml *)

(** Representation of the integers. *)
module Int = struct
  type t = int
  let leq a b = a <= b
end

(** Limits of the precise representation. *)
module Limits = struct
  type t = Int.t
  let low = -100
  let high = 100
  let in_range x =
    assert (Int.le low high)
end

(** Lower bound representation. *)
module Lower = struct
  type t = MinInf | Int of Int.t
  let data x =
    
end

(** Extension of the data with a lowest and highest value. *)
module Bound = struct
  type t =
  | Below
  | Data of Data.t
  | Above
  let leq a b = match (a,b) with
    | _, Below
    | Above,_ -> false
    | Below, _
    | _,Above -> true
    | Data da, Data db -> Data.leq da db
  let min a b = if leq a b then a else b
  let max a b = if leq a b then b else a
  let to_data = function
    | Below -> Data.min_value
    | Data x -> x
    | Above -> Data.max_value
end

(** Intervals on the data (bounded or unbounded).
    No representation is given for the empty interval. *)
module Interval = struct
  type t = { low : Bound.t; high : Bound.t }
  exception Bottom
  (** Triggered when building an empty interval. *)
  let make low high =
    if Bound.leq low high
    then { low = low; high = high }
    else raise Bottom
  let unbounded = make Bound.Below Bound.Above
  let lower_bounded l = make (Bound.Data l) Bound.Above
  let upper_bounded h = make Bound.Below (Bound.Data h)
  let bounded l h = make (Bound.Data l) (Bound.Data h)
  let singleton x = make (Bound.Data x) (Bound.Data x)
  let join a b = make (Bound.min a.low b.low) (Bound.max a.high b.high)
  let meet a b = make (Bound.max a.low b.low) (Bound.min a.high b.high)
  let is_unbounded i = i = unbounded
  let to_data_range i = Bound.to_data i.low, Bound.to_data i.high
end

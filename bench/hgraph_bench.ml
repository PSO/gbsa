
let time_exec op =
  let t0 = Sys.time () in
  op ();
  let tn = Sys.time () in
  tn -. t0

let time_line n =
  let op () =
    ignore (Hgraph_gen.line n) in
  let t = time_exec op in
  Format.eprintf "line %d in %f s@\n@?" n t;
  Format.printf "line %d; %f@\n@?" n t

let _ =
  Format.printf "Bench; Time (s)@\n@?";
  for i = 1 to 100
  do
    time_line (100 * i)
  done

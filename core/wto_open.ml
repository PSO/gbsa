(**
   Elements required to build weak topological ordering.
*)

open Wto_types

(** {3 Subtypes Modules} *)

(** {7 Pre-modules functions} *)

let rec print_component_heads print_node fmt c =
  Format.fprintf fmt "@,[@[<hov 2>%a@,%a]@]@,"
    print_node c.head
    (print_fragment_heads print_node) c.tail
and print_fragment_heads print_node fmt f =
  List.iter (print_component_heads print_node fmt) f.subs

let rec print_component print_node fmt c =
  Format.fprintf fmt "@,[@[<hov 2>%a@,%a]@]@,"
    print_node c.head
    (print_fragment print_node) c.tail
and print_fragment print_node fmt f =
  let print_element fmt e = match e with
    | Node n -> print_node fmt n
    | Component c -> print_component print_node fmt c in
  List.iter (print_element fmt) f.content

let fragment_of_list l =
  let subs = List.fold_right (fun x acc -> match x with
    | Node _ -> acc
    | Component c -> c::acc) l [] in
  { content = l; subs = subs }

let rec map_component f c =
  { head = f c.head;
    tail = map_fragment f c.tail }
and map_fragment f frag =
  let map_element = function
    | Node n -> Node (f n)
    | Component c -> Component (map_component f c) in
  fragment_of_list (List.map map_element frag.content)

(** {7 Modules} *)

module Element = struct
  type 'a t = 'a element
  let of_node n = Node n
  let of_component c = Component c
end

module Fragment = struct
  type 'a t = 'a fragment
  let of_list = fragment_of_list
  let of_node_list l = of_list (List.map (fun n -> Node n) l)
  let of_component c = { content = [Component c]; subs = [c] }
  let map = map_fragment
  let content f = f.content
  let fold fnode frag acc =
    let rec frag_fold env rank frag acc =
      List.fold_left (fun (acc,rank) e -> match e with
      | Node n -> fnode (rank,false,env) n acc, rank + 1
      | Component c -> comp_fold (c::env) rank c acc) (acc,rank) frag.content
    and comp_fold env rank comp acc =
      let acc = fnode (rank,true,env) comp.head acc in
      frag_fold env (rank + 1) comp.tail acc in
    fst (frag_fold [] 0 frag acc)
  let fold_backward fnode frag acc =
    let rec frag_fold_back env frag acc =
      List.fold_right (fun e acc -> match e with
      | Node n -> fnode (false,env) n acc
      | Component c -> comp_fold_back (c::env) c acc) frag.content acc
    and comp_fold_back env comp acc =
      let acc = frag_fold_back env comp.tail acc in
      fnode (true,env) comp.head acc in
    frag_fold_back [] frag acc
  let fold_components fcomp frag acc =
    let rec frag_fold frag acc =
      List.fold_left (fun acc c ->
	let acc = fcomp c acc in
	frag_fold c.tail acc) acc frag.subs in
    frag_fold frag acc
  let fold_components_backward fcomp frag acc =
    let rec frag_fold_back frag acc =
      List.fold_right (fun c acc ->
	let acc = frag_fold_back c.tail acc in
	fcomp c acc) frag.subs acc in
    frag_fold_back frag acc
  let fold_elements_after fn fc =
    let rec rf f acc =
      List.fold_left (fun acc e -> match e with
      | Node n -> fn n acc
      | Component c ->
	fc c (rf c.tail (fn c.head acc))) acc f.content in
    rf
		     
  (* Compatibility with mk_wto. *)
  let empty = { content = []; subs = [] }
  let cons_node e f = { f with content = (Node e)::f.content }
  let cons_frag s f =
    match s.content with
    | Node h::r ->
      let st = { s with content = r } in
      let c = { head = h; tail = st } in
      { content = (Component c)::f.content;
	subs = c::f.subs }
    | _ -> failwith "Ill-formed sub-fragment (should not be empty or start by a component)"
  let append f1 f2 = { content = List.append f1.content f2.content;
		       subs = List.append f1.subs f2.subs }
  let print_heads = print_fragment_heads
  let print = print_fragment
end

module Component = struct
  type 'a t = 'a component
  let of_fragment f = match f.content with
    | (Node h)::t -> { head = h; tail = { content = t; subs = f.subs } }
    | (Component _)::_ -> raise (Invalid_argument "components cannot start with a component")
    | [] -> raise (Invalid_argument "components cannot be empty")
  let of_list l = of_fragment (Fragment.of_list l)
  let head c = c.head
  let content c = (Node c.head)::(Fragment.content c.tail)
  let fold fnode c acc = Fragment.fold fnode (Fragment.of_component c) acc
  let print_heads = print_component_heads
  let map = map_component
  let nodes c = fold (fun _ n acc -> Sette.add n acc) c Sette.empty
end

(** {7 Post-modules functions} *)

(** Totally ordered sets.

    @image total_order.tex.png
    @author pascal.sotin at irit.fr
*)

(** Ordering two elements. *)
module Ordering : sig
  (** Possible orderings between two elements. *)
  type t = 
  | LT (** First less than the second. *)
  | EQ (** First equal to the second. *)
  | GT (** First greater than the second. *)

  (** Ordering to string. *)
  val to_string : t -> string
    
  (** Ordering printer. *)
  val print : Format.formatter -> t -> unit   
end

(** {2 Building total orders} *)

(** A total order on a set of elements of type ['a].

    Elements are internally used as keys for a map
    (comparison using [Pervasives.compare]).
*)
type 'a t

(** From a non-empty list. *)
val of_list : 'a list -> 'a t

(** From a non-empty {e immutable} array. *)
val of_array : 'a array -> 'a t

(** Concatenates two orders. *)
val append : 'a t -> 'a t -> 'a t

(** Reverses an order. *)
val rev : 'a t -> 'a t

(** {2 Order characteristics} *)

(** Number of elements in the order. *)
val cardinal : 'a t -> int

(** Lowest element of the order. *)
val bot : 'a t -> 'a

(** Highest element of the order. *)
val top : 'a t -> 'a

(** {2 Using total orders} *)

(** {5 Functions on two elements} *)

(** Less or equal. *)
val le : 'a t -> 'a -> 'a -> bool

(** Strictly less than. *)
val lt : 'a t -> 'a -> 'a -> bool

(** Greater or equal. *)
val ge : 'a t -> 'a -> 'a -> bool

(** Strictly greater than. *)
val gt : 'a t -> 'a -> 'a -> bool

(** Equal. *)
val eq : 'a t -> 'a -> 'a -> bool

(** Minimum. *)
val min : 'a t -> 'a -> 'a -> 'a

(** Minimum. *)
val max : 'a t -> 'a -> 'a -> 'a

(** Comparison function. *)
val compare : 'a t -> 'a -> 'a -> int

(** Ordering function. *)
val order : 'a t -> 'a -> 'a -> Ordering.t

(** {5 Functions on lists of elements} *)

(** Least upper bound. [bot] if the list is empty. *)
val lub : 'a t -> 'a list -> 'a

(** Greatest lower bound. [top] if the list is empty. *)
val glb : 'a t -> 'a list -> 'a

(** {5 Navigation in the order} *)

(** Next element in the order ([None] if the argument is [top]). *)
val next : 'a t -> 'a -> 'a option

(** Previous element in the order ([None] if the argument is [bot]). *)
val prev : 'a t -> 'a -> 'a option

(** Moves of several ranks in the order. Moves upward if the parameter is positive, downward otherwise.
    Returns [None] when leaving the order. *)
val shift : 'a t -> int -> 'a -> 'a option

(** {5 Mapping with [0..cardinal-1]} *)

(** Mapping to [0..cardinal-1] *)
val rank : 'a t -> 'a -> int

(** Building from [0..cardinal-1] *)
val element : 'a t -> int -> 'a

(** {5 Other representations} *)

(** As a set. The enumeration order of the set follows the total order. *)
val to_set : 'a t -> 'a PSette.t

(** As a list. The order of the list follows the total order. *)
val to_list : 'a t -> 'a list

(** {5 Fold and iter} *)

(** Upward iteration. *)
val iter : ('a -> unit) -> 'a t -> unit

(** Upward iteration with ranks. *)
val iteri : (int -> 'a -> unit) -> 'a t -> unit

(** Downward iteration. *)
val iter_down : ('a -> unit) -> 'a t -> unit

(** Downward iteration with decreasing ranks. *)
val iteri_down : (int -> 'a -> unit) -> 'a t -> unit

(** Upward fold. *)
val fold : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b

(** Upward fold with ranks. *)
val foldi : (int -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

(** Downward fold. *)
val fold_down : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b

(** Downward fold with decreasing ranks. *)
val foldi_down : (int -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

(** {5 Searching} *)

(** Finds the smallest element satisfying the predicate. *)
val find : ('a -> bool) -> 'a t -> 'a

(** Finds the largest element satisfying the predicate. *)
val find_down : ('a -> bool) -> 'a t -> 'a

(** Looks for an image different from [None], starting from [bot] up to [top]. The ranks are provided. *)
val lookup : (int -> 'a -> 'b option) -> 'a t -> 'b option

(** Looks for an image different from [None], starting from [top] down to [bot]. Decreasing ranks are provided. *)
val lookdown : (int -> 'a -> 'b option) -> 'a t -> 'b option


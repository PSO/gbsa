(**
   Hyper-graphs implementation.
*)

include Hgraph_types
include Hgraph_hedge
include Hgraph_inspect
include Hgraph_maker
include Hgraph_unfold
include Hgraph_reach
include Hgraph_compress
include Hgraph_dot

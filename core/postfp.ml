(** Post-fixpoint implementation. *)

module Widening = struct
  type 'a t =
  [ `Stable of 'a -> 'a -> 'a
  | `Widen of 'a -> 'a -> 'a
  ]

  let mk_stablei ~le (w : 'a t) = match w with
    | `Stable w -> fun _ -> w
    | `Widen w -> fun _ x y -> if le y x then y else w x y

  let geti (w : 'a t) = match w with
    | `Stable w -> fun _ -> w
    | `Widen w -> fun _ -> w
end

type 'a iterates = ?fmt:Format.formatter -> int -> 'a -> 'a -> [ `Stable | `Next of 'a ]

let asc_iterates ~le ~widen =
  let widen = Widening.mk_stablei ~le widen in
  fun ?fmt i x y ->
    if x == y then `Stable
    else begin
      begin match fmt with
      | None -> ()
      | Some fmt -> Format.fprintf fmt "Performing a widening@\n"
      end;
      let w = widen i x y in
      if w == x || le w x then `Stable else `Next w
    end

let asc_iterates_limited_desc ~le ~widen ~limit =
  let desc = ref limit in
  let widen = Widening.geti widen in
  fun ?fmt i x y ->
    if x == y then `Stable (* obviously stable *)
    else begin
      if le y x
      then begin
	if !desc = 0 then `Stable (* post-fixpoint, desc limit reached *)
	else begin
	  if le x y
	  then `Stable (* stable *)
	  else begin
	    begin match fmt with
	    | None -> ()
	    | Some fmt -> Format.fprintf fmt "Descending@\n"
	    end;
	    decr desc;
	    `Next y (* descending *)
	  end
	end
      end
      else begin
	begin match fmt with
	| None -> ()
	| Some fmt -> Format.fprintf fmt "Performing a widening@\n"
	end;
	`Next (widen i x y)
      end
    end

(** We compute the sequence:
    {ul 
    {li [X(0) = base],}
    {li [X(n) = f( X(n-1) )].
    }
    Stabilisation is detected by [X(n+1) <= X(n)].
*)
let kleene ~le ~pre f =
  let rec rfp x =
    let x' = f x in
    if le x' x
    then x
    else rfp x' in
  `Fp (rfp pre)

let asc ~widen ~le ~pre f =
  let widen = Widening.mk_stablei ~le widen in
  let rec ra n x =
    let fx = f x in
    let wfx = widen n x fx in
    if wfx == x || le wfx x then wfx else ra (n + 1) wfx in
  `Postfp (ra 0 pre)

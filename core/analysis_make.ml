open Analysis_widening
open Analysis_dom

(** Functor for the analysis. *)

module Make = functor (D : DOM) -> struct 
  type node = D.node
  type label = D.label
  let analysis w c =
    let fmt = Format.std_formatter in
    let bot = D.bot c in
    let join = D.join in
    let iterates comp = Postfp.asc_iterates_limited_desc ~le:D.le ~widen:(D.widen c comp) ~limit:1 in
    if D.narrow then failwith "Not handled yet: narrowing"; (* Not really sure on how to handle that. *)
    let interpret = D.interpret c in
    Wgraph.Analysis.postfp
      ~pp_node:D.print_node ~pp_label:D.print_label ~pp_value:D.print_abs
      ~fmt
      w
      ~bot ~join ~iterates ~interpret
end


	

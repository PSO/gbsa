(** Hyper-edges submodule. *)

open Hgraph_types

(** Hyper-edges implementation. *)
module Hedge = struct
  type ('a,'b) t = ('a,'b) hedge
  let mapf ~src ~dst ~lab h = 
    { src = List.mapi src h.src; dst = dst h.dst ; lab = lab h.lab }
  let mapi ~src ~dst h = { h with src = List.mapi src h.src; dst = dst h.dst }
  let map f = mapi ~src:(fun _ -> f) ~dst:f
  let src_set h = List.fold_left (fun s n -> Sette.add n s) Sette.empty h.src
  let depends_on n h = List.mem n h.src
  let make src lab dst = { src = src; lab = lab; dst = dst }
  let of_tuple (src,lab,dst) = make src lab dst
  let src h = h.src
  let dst h = h.dst
  let lab h = h.lab
  let arity h = List.length (src h)
  let print print_node print_label fmt h =
    Format.fprintf fmt "%a-%a->%a"
      (Print.list print_node) h.src
      print_label h.lab
      print_node h.dst
  let equal ?eq_label h1 h2 = match eq_label with
    | None -> h1 = h2
    | Some eq_label ->
      h1.src = h2.src && h1.dst = h2.dst && eq_label h1.lab h2.lab
  let project_nodes f h = of_tuple (List.map f (src h), lab h, f (dst h))
  type 'a predicate = 'a -> bool
  let dst_sat p h = p h.dst
  let lab_sat p h = p h.lab
  let any_src p h = List.exists p h.src
  let all_src p h = List.for_all p h.src
  let any_node p h = (dst_sat p h) || (any_src p h)
  let all_nodes p h = (dst_sat p h) && (all_src p h)
  let all_src_dst p2 h = all_src (fun src -> p2 src h.dst) h
  let any_src_dst p2 h = any_src (fun src -> p2 src h.dst) h
    
  let lift_is hp = fun y -> hp (fun x -> x = y)
  let dst_is y = lift_is dst_sat y
  let any_src_is y = lift_is any_src y
  let any_node_is y = lift_is any_node y
    
  let lift_in hp = fun s -> hp (fun x -> Sette.mem x s)
  let dst_in s = lift_in dst_sat s
  let any_src_in s = lift_in any_src s
  let all_src_in s = lift_in all_src s
  let any_node_in s = lift_in any_node s
  let all_nodes_in s = lift_in all_nodes s
    
  let is_init h = h.src = []
  let is_not_init h = h.src <> []

  let arity_eq n h = arity h = n
  let arity_le n h = arity h <= n
  let arity_ge n h = arity h >= n
end

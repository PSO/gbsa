
module Body = struct
  module Node = struct
    type tag = [`Param | `Before | `Body | `After]
    type 'a t = 'a * tag
    let classify = snd
    let origin = fst
    let make orig tag = orig, (tag :> tag)
  end
  type ('a, 'b) t = {
    graph : ('a Node.t, 'b) Wgraph.t;
    head : 'a;
    parameters : 'a Sette.t;
  }
  let graph body = body.graph
  let head bef_aft body = Node.make body.head bef_aft
  let parameters b = Sette.fold (fun p -> Sette.add (Node.make p `Param)) b.parameters Sette.empty
end

module Frame = struct
  module Node = struct
    type tag = [`Original | `Before | `After]
    type 'a t = 'a * tag
    let classify = snd
    let origin = fst
    let make orig tag = orig, (tag :> tag)
  end
  module Label = struct
    type 'b t = [`Original of 'b | `Bodies]
  end
  type ('a, 'b) t = {
    graph : ('a Node.t, 'b Label.t) Wgraph.t;
    head : 'a;
  }
  let graph frame = frame.graph
  let head bef_aft frame = Node.make frame.head bef_aft
end

type ('a, 'b) t = {
  body : ('a, 'b) Body.t;
  frame : ('a, 'b) Frame.t;
}
let body loop = loop.body
let frame loop = loop.frame
let extract wg ~head =
  let graph = Wgraph.graph wg in
  let wto = Wgraph.wto wg in
  
  (* Retreiving the component. *)
  let comp = match Wto.node_components wto head with
    | c::_ when Wto.Component.head c = head -> c
    | _ -> raise (Invalid_argument "not a component head") in
  let comp_nodes = Wto.Component.nodes comp in

  (* Computing node sets. *)
  let analysis direction =
    let forward, backward = match direction with
      | `Forward -> Hgraph.reachable, Hgraph.coreachable
      | `Backward -> Hgraph.coreachable, Hgraph.reachable in
    let inside node = Sette.mem node comp_nodes in
    let outside node = not (inside node) in
    let is_head node = (node = head) in
    (* Reachability (or coreachability) from the head. *)
    let reach = forward
      ~frontier:outside graph (Sette.singleton head) in
    let out, comp = Sette.partition outside reach in
    let outside_reach node = not (Sette.mem node comp_nodes) in
    (* Coreachability (or reachability) from the outside. *)
    let back = backward
      ~excluded:outside_reach ~frontier:is_head graph out in
    let back = Sette.filter inside back in
    comp, out, back
  in
  let f_comp, output, after = analysis `Forward in
  let b_comp, input, before = analysis `Backward in
  let body = Sette.inter f_comp b_comp in

  failwith "todo"

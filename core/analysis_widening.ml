(** Controlling the widening. *)

module Widening = struct
  type 'a result = { value : 'a; halt : bool }
  type 'a t = { widen : 'a -> 'a -> 'a result }
  let result value ~halt = { value = value; halt = halt }
  let basic ~le ~join =
    let w x y =
      let value, halt = if le y x then y, true else join x y, false in
      result value ~halt in
    { widen = w }
	
end

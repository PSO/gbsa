(** Using hyper-graphs. *)

open Hgraph_types
open Hgraph_hedge

(** {3 Printing} **)

let dump dump_node dump_label fmt g =
  let dump_inc_h = Mappe.print dump_node (Print.list (Hedge.print dump_node dump_label)) in
  let dump_suc_n = Mappe.print dump_node (Sette.print dump_node) in
  Format.fprintf fmt "<@[inc_g=%a,@ suc_n=%a@]>"
    dump_inc_h g.inc_h
    dump_suc_n g.suc_n

let print print_node print_label fmt g =
  Mappe.iter (fun _ incoming -> Print.list ~first:"" ~sep:"@\n" ~last:"@\n" (Hedge.print print_node print_label) fmt incoming) g.inc_h

(** {3 Inspectors} *)

let is_empty g = Mappe.is_empty g.inc_h

let node_count g = Mappe.cardinal g.inc_h

let hedge_count g = Mappe.fold (fun _ inc ct -> ct + List.length inc) g.inc_h 0

let nodes g = Mappe.maptoset g.inc_h

let hedges g = Mappe.fold (fun _ inc acc -> List.append inc acc) g.inc_h []

let incoming g n = Mappe.find n g.inc_h

let succ g n = Mappe.find n g.suc_n

let pred g n = List.fold_left (fun acc h -> Sette.union (Hedge.src_set h) acc) Sette.empty (incoming g n)

let roots g = Mappe.fold (fun n inc acc ->
  if List.exists Hedge.is_not_init inc
  then acc
  else Sette.add n acc) g.inc_h Sette.empty

let outgoing g n = Sette.fold (fun s acc -> List.append (List.filter (Hedge.depends_on n) (incoming g s)) acc) (succ g n) []

let contains g n = Mappe.mem n g.inc_h

(** {3 Folds} *)

let fold_nodes f g acc = Mappe.fold (fun n _ acc -> f n acc) g.inc_h acc

let fold_hedges f g acc = Mappe.fold (fun _ l acc -> List.fold_left (fun acc h -> f h acc) acc l) g.inc_h acc

let lift_i fold = fun f g acc -> fst (fold (fun n (acc,i) -> f i n acc, i+1) g (acc,0))

let foldi_nodes f g acc = lift_i fold_nodes f g acc

let foldi_hedges f g acc = lift_i fold_hedges f g acc

let iter_nodes f g = fold_nodes (fun n () -> f n) g ()

let iteri_nodes f g = foldi_nodes (fun i n () -> f i n) g ()

let iter_hedges f g = fold_hedges (fun n () -> f n) g ()

let iteri_hedges f g = foldi_hedges (fun i n () -> f i n) g ()

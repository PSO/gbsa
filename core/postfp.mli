(** Computing post-fixpoints of a monotone function.

    @image postfp.tex.png
    @author pascal.sotin at irit.fr
*)

(** {3 Widening and narrowing} *)

(** The Graph-Based Static Analysis library rely on widening and narrowing operators satisfying the requierements stated by Patrick Cousot and Radhia Cousot.
*)

(** Widening operator. *)
module Widening : sig

  (**
     A widening is a binary operator [widen] satisfying:
     {ul
     {li [join x y <= widen x y],}
     {li for all increasing chain [x(i)], the increasing chain [y(i)] defined by [y(0) = x(0)] and [y(i+1) = widen y(i) x(i+1)] is not strictly increasing.}
     }
     If in addition, if [y <= x] implies [widen x y = x] then it is said to be {e stable}.
  *)

  type 'a t =
  [ `Stable of 'a -> 'a -> 'a
  | `Widen of 'a -> 'a -> 'a
  ]

end

(** {3 Iteration techniques} *)

type 'a iterates = ?fmt:Format.formatter -> int -> 'a -> 'a -> [ `Stable | `Next of 'a ]

val asc_iterates : le:('a -> 'a -> bool) -> widen:('a Widening.t) -> 'a iterates

val asc_iterates_limited_desc : le:('a -> 'a -> bool) -> widen:('a Widening.t) -> limit:int -> 'a iterates

(** {3 Full control iterations} *)

(** Kleene iterations.

    [kleene ~le ~pre f] computes the least fixpoint of the monotone function [f] greater or equal to [pre].
    {ul
    {li The parameter [le] gives the lattice ordering ([le a b] means [a <= b]),}
    {li The parameter [pre] must be a pre-fixpoint ([pre <= f pre]).
    If [pre] is the bottom element of the lattice, the least fixpoint is returned,}
    {li May run {e forever} if the lattice contains infinite ascending chains.}
    }
 *)
val kleene : le:('a -> 'a -> bool) -> pre:'a -> ('a -> 'a) -> [`Fp of 'a]

(** Accelerated ascending iterations.

    [asc ~widen ~le ~pre f] computes an over-approximation of the least fixpoint of the monotone function [f]
    greater or equal to [pre] (ie. a post-fixpoint of [f] greater or equal to [pre]).
    {ul
    {li The parameter [le] gives the lattice ordering ([le a b] means [a <= b]),}
    {li The parameter [pre] must be a pre-fixpoint ([pre <= f pre]).
    It is typically the bottom element of the lattice,}
    {li Termination is ensured by the widening operator [widen].}
    }
*)
val asc : widen:('a Widening.t) -> le:('a -> 'a -> bool) -> pre:'a -> ('a -> 'a) -> [`Postfp of 'a]

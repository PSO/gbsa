
open Wto_types
open Wto_closed

(** {3 Analysis OF a WTO} *)

let rec fixpoint f x =
  let y = f x in
  if x = y then y else fixpoint f y

let backward
    (w : 'a t)
    (le : 'b -> 'b -> bool)
    (bot : 'b)
    (join : 'b -> 'b -> 'b)
    (back_eval : 'a -> bool -> 'b -> 'b)
    :
    'b -> 'b
    =
  let rec rf f =
    List.fold_right re f.content
  and re e = match e with
    | Node n -> back_eval n false
    | Component c -> rc c
  and rc c out_iter =
    let rb out_body =
	(back_eval c.head true (rf c.tail (join out_iter out_body))) in
    let rec postfix out_body =
      let in_body = rb out_body in
      if le in_body out_body then out_body else postfix in_body in
    postfix bot
  in
  rf w.root

(*
module Liveness = struct
  type 'a t = 'a Sette.t
  let join = Sette.union
  let bot = Sette.empty
  let le = Sette.subset
  let back_eval hg n w out =
    let b_assign out = Sette.remove n out in
    let b_widen out = if w then Sette.add n out else out in
    let b_hedge he out =
      Sette.union out
	(List.fold_right Sette.add (Hgraph.Hedge.src he) Sette.empty) in
    let b_hedges out = List.fold_right b_hedge (Hgraph.incoming hg n) out in
    let res = b_hedges (b_widen (b_assign out)) in
    Format.printf "Live starting %d = %a@\n" n (Sette.print Format.pp_print_int) res;
    res
end

let liveness w hg = backward w Liveness.le Liveness.bot Liveness.join (Liveness.back_eval hg) Liveness.bot
*)

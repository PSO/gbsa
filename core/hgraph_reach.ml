(** Function unfolding *)

open Hgraph_types
open Hgraph_hedge
open Hgraph_inspect

let work_set_algo
    ?(excluded=(fun _ -> false))
    ?(frontier=(fun _ -> false))
    gen s =
  let rec rp ss ws rs =
    if Sette.is_empty ws
    then rs
    else
      let n = Sette.choose ws in
      let ws = Sette.remove n ws in
      if excluded n
      then rp ss ws rs
      else
	let rs = Sette.add n rs in
	let ss, ws = if frontier n
	  then ss, ws
	  else
	    let ns = gen n in
	    Sette.union ss ns,
	    Sette.union ws (Sette.diff (gen n) ss) in
	rp ss ws rs
  in
  rp s s Sette.empty

let coreachable ?excluded ?frontier g =
  work_set_algo ?excluded ?frontier (pred g)

let reachable ?excluded ?frontier g =
  work_set_algo ?excluded ?frontier (succ g)

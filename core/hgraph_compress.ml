(** Compression of graph. *)

open Hgraph_types
open Hgraph_hedge
open Hgraph_inspect
open Hgraph_maker

(**
   Builds a graph using only a subset of the nodes of the initial graph by compressing several labels
   on the same edges. The nodes are removed iteratively until no node has a single incoming hyper-hedge
   and a single outgoing one-to-one edge (unless it is an auto-loop). *)
let compress_generic ?protect ~lift ~prepend g =
  let protect = match protect with
    | None -> fun _ -> false
    | Some f -> f in

  let process_node n incs (treated, res) =

    (* Node "a" is simply linked to node "b" if
       only one edge goes from "a" to "b" and if this edge is one-to-one. *)
    let simple_link a incs_b =
      let rec rscan = function
	| h::r -> begin
	  match h.src with
	  | [x] when x = a -> (* one one-to-one edge from a to b *)
	    if List.exists (fun h -> List.mem a h.src) r
	    then None (* other edge from a to b *)
	    else Some h
	  | l ->
	    if List.mem a l
	    then None (* hyper-edge >1 from a to b *)
	    else rscan r end
	| [] -> None in
      rscan incs_b
    in

    (* A node is incompressible if either:
       - it is protected
       - it has several or no incoming edges
       - it has several or no outgoing edges
       - it has an outgoing hyper-edge of arity 2 or more
       - it has the processed node as successor *)
    let rec find_incompressible cur incs =
      let found = cur, incs in
      if protect cur
      then found
      else match incs with
      | [_] -> (* unique incoming edge *)
	let suc = Mappe.find cur g.suc_n in
	if Sette.cardinal suc <> 1
	then found (* 0 or many successors *)
	else (* unique successor *)
	  let s = Sette.choose suc in
	  if s = n
	  then found (* the successor is the starting node *)
	  else
	    let incs_s = Mappe.find s g.inc_h in
	    begin match simple_link cur incs_s with
	    | Some _ -> find_incompressible s incs_s
	    | None -> found (* outgoing hyper-edge or several outgoing edges *)
	    end
      | _ -> found (* several incoming edges *)
    in

    (* We have a simple link between src and dst.
       We extend this simple link backward and add it to the graph. *)
    let compress_backward res dst labels src =
      let rec rcomp treated labels src =
	let stop_here () = treated, add_edge res src labels dst in
	if protect src
	then stop_here ()
	else match Mappe.find src g.inc_h with
	| [h] -> (* unique incoming edge *)
	  let labels = prepend h.lab labels in
	  let treated = Sette.add src treated in
	  begin match h.src with
	  | [src] -> (* one-to-one *)
	    rcomp treated labels src
	  | src_h -> treated, add_hyper res src_h labels dst end
	| _ -> stop_here () (* multiple incoming edges *)
      in
      rcomp Sette.empty labels src
    in

    (* We have an incompressible node.
       We process its incoming edges. *)
    let process_incomp res n incs =
      let (_,multi) =
	List.fold_left (fun acc h ->
	  List.fold_left (fun (seen,multi) p ->
	    if Sette.mem p seen
	    then (seen, Sette.add p multi)
	    else (Sette.add p seen, multi)) acc h.src)
	  (Sette.empty,Sette.empty)
	  incs in
      List.fold_left (fun (treated,res) h ->
	let label = lift h.lab in
	match h.src with
	| [p] when not (Sette.mem p multi) ->
	  let (t,r) = compress_backward res n label p in
	  Sette.union treated t, r
	| src_h -> treated, add_hyper res src_h label n)
	(Sette.singleton n, res)
	incs
    in

    (* True node processing *)
    if Sette.mem n treated
    then treated, res
    else
      let (incomp_n, incs) = find_incompressible n incs in
      let (treated_by,res) = process_incomp res incomp_n incs in
      assert (Sette.is_empty (Sette.inter treated treated_by));
      Sette.union treated treated_by, res
  in

  let (t,r) = Mappe.fold process_node g.inc_h (Sette.empty, empty) in
  assert (Sette.equal t (nodes g));
  r

let compress ?protect g = compress_generic ?protect ~lift:(fun x -> [x]) ~prepend:(fun x y -> x::y) g

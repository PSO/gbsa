open Hgraph_hedge
open Hgraph_inspect

let dump_dot pn pl ?(close=true) g oc =

  let print = output_string oc in
  let lf = Format.make_formatter
    (fun str pos len ->
      for i = pos to pos + len - 1 do
	match String.get str i with
	| '"' -> output_string oc "\\\""
	| c -> output_char oc c
      done)
    (fun () -> flush oc) in

  let print_node i n =
    Printf.fprintf oc "  n%d [label=\"%!" i;
    Format.fprintf lf "%a%!" pn n;
    print "\"]\n"
  in

  let print_hedge names i h =

    let id n = Mappe.find n names in

    Printf.fprintf oc "  h%d [label=\"%!" i;
    Format.fprintf lf "%a%!" pl (Hedge.lab h);
    Printf.fprintf oc "\"]; ";
    List.iter
      (fun n ->	Printf.fprintf oc "n%d -> h%d [arrowhead=none]; " (id n) i)
      (Hedge.src h);
    Printf.fprintf oc "h%d -> n%d\n" i (id (Hedge.dst h))
  in

  print "digraph {\n";
  let names = foldi_nodes (fun i n names -> print_node i n; Mappe.add n i names) g Mappe.empty in
  print "  node [shape=rectangle, color=blue, margin=0]; edge [color=blue]\n";
  iteri_hedges (print_hedge names) g;
  print "}\n";
  if close
  then close_out oc
  else flush oc

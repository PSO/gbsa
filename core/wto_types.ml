(**
   Weak topological ordering representation.
*)

(** {3 Types} *)

type rank = int

type 'a element =
| Node of 'a                (** A regular node *)
| Component of 'a component (** A component *)
and 'a fragment = {
  content : 'a element list; (** The complete content list. *)
  subs : 'a component list;  (** The components of the content list, for faster descent. *)
}
and 'a component = {
  head : 'a;          (** Head node *)
  tail : 'a fragment; (** Remainder of the component *)
}

type 'a wto = {
  root : 'a fragment;                       (** Root fragment *)
  nodes : ('a, 'a component list) Mappe.t;  (** Enclosing components *)
  ranks : 'a TotalOrder.t;                  (** Underlying total ordering *)
}




(*
(** {3 Testing} *)

let a_b_c =
  let c = component_of_list ['c'] in
  let b_c = component_of_element_list [Node 'b'; Component c] in
  let a_b_c = fragment_of_element_list [Node 'a'; Component b_c] in
  a_b_c

let recursive_test () =
  let wto = of_fragment a_b_c in
  let update x _ = Format.printf "updating '@@%c'@\n" x; x in
  let widen_controler _ x y = Format.printf "widening '%c' '%c'@\n" x y; y in
  let le x y = Format.printf "Testing '%c' le '%c'@\n" x y; x = y in
  let ini = fun _ -> 'x' in
  recursive wto update widen_controler le ini

let _ = recursive_test ()
*)

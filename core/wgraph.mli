(** A {!Hgraph} paired with a suitable {!Wto}.

   @image wto.tex.png
   @author pascal.sotin at irit.fr
*)

(** Main type. *)
type ('a,'b) t

(** {3 Building} *)

(** From an hyper-graph. The wto is computed using Bourdoncle algorithm. *)
val of_graph : ?entry:'a -> ('a,'b) Hgraph.t -> ('a,'b) t

(** From an hyper-graph and a suitable wto. *)
val make : ('a,'b) Hgraph.t -> 'a Wto.t -> ('a,'b) t

(** Adds a zero-to-one edge to the graph. The wto is unchanged. *)
val add_init : ('a,'b) t -> 'b -> 'a -> ('a,'b) t

(** {3 Inspecting} *)

(** The underlying graph *)
val graph : ('a,'b) t -> ('a,'b) Hgraph.t

(** The underlying wto *)
val wto : ('a,'b) t -> 'a Wto.t

(** The list of component heads *)
val heads : ('a,'b) t -> 'a list

(** {3 Analysis} *)

module Analysis : sig
  val postfp :
    ?pp_node:(Format.formatter -> 'a -> unit) ->
    ?pp_label:(Format.formatter -> 'b -> unit) ->
    ?pp_value:(Format.formatter -> 'c -> unit) ->
    ?fmt:Format.formatter ->
    ('a,'b) t ->
    bot:('a -> 'c) ->
    join:('c -> 'c -> 'c) ->
    iterates:('a Wto.component -> 'c Postfp.iterates) ->
    interpret:(('a * 'c) array -> 'b -> 'a -> 'c) ->
    ('a,'c) Mappe.t
end

(** {7 Component extraction} *)

(*
(* Peut etre mieux si c'est un module a part de WGraph *)
module Extract : sig
    
end
*)

(**
   A Wgraph representing an input output relation.
   Its wto is of the form [inputs locals outputs] where:
   {ul
   {li [inputs] contains nodes tagged by [`Input] (no hierarchy),}
   {li [outputs] contains nodes tagged by [`Output] (no hierarchy),}
   {li [locals] contains all the nodes of the component, tagged by [`Local] and possibly hierarchized with sub-components.}
   }
   A node might appear both tagged by [`Input] and [`Output]. Input nodes have no incoming edges. Output nodes have no outgoing edges.
*)
type ('a,'b) relation = ('a * [`Input | `Local | `Output], 'b) t

(** Extracts a component indicated by its head as a relation.
    The [local] section of this relation is isomorphic to the target component.
    Also returns the set of inputs and outputs. *)
val extract_component : ('a,'b) t -> head:'a -> ('a,'b) relation * (('a * [`Input]) Sette.t * ('a * [`Output]) Sette.t)

(**
   A Wgraph representing the body of a component.
   This graph contains all nodes that are both reachable and co-reachable from the head of the component.
   The head is duplicated so to "open" the component. Tags are interpreted as follows:
   {ul
   {li [`Body] marks node both reachable and co-reachable from the head (including the head itself),}
   {li [`Input] and [`Local] mark nodes involved in hyper-edges targetting nodes of the body
   and depending both on nodes inside and outside the body. }
   }
   The head appears twice, tagged both as [`Local] and [`Body], denoting respectively the states before and after execution of the body.
   [`Input] and [`Local] nodes have no incoming edges. The head tagged by [`Body] has no outgoing edges.
*)
type ('a,'b) body = ('a * [`Input | `Pre | `Body | `Local], 'b) t


(**
   A Wgraph representing a component which body has been encapsulated in a single hyper-edge.
   Its nodes are tagged:
   {ul
   {li [`Input], [`Local], [`Output] are interpreted as in an extracted component,}
   {li [`Pre] marks copies of nodes of the body, before the head is reached. }
   }
   Labels are also adapted:
   {ul
   {li [`Regular lab] has to be interpreted as the original label [lab],}
   {li [`Id] has to be interpreted as the identity function,}
   {li [`Graph] carries the body of the component.}
   }
   The head appears twice, tagged both as [`Pre] and [`Local], denoting respectively the states before and after fixpoint computation.
   The [`Pre] head and the [`Local] head are linked by an edge labelled by [`Id].
   The [`Local] head is linked to itseld by an edge labelled by the sub-graph correponding to the body.
*)
type ('a,'b) frame = ('a * [`Input | `Pre | `Local | `Output],
		      [`Regular of 'b | `Bodies]) t

type ('a,'b) extraction =
  {
    pre_head : 'a * [`Pre];
    loc_head : 'a * [`Local];
    body : ('a,'b) body;
    frame : ('a,'b) frame;
    input : ('a * [`Input]) Sette.t;
    output : ('a * [`Output]) Sette.t;
  }

(** Extracts a component indicated by its head as a pair [body * frame].
    Also returns the set of inputs and outputs. *)
(* val extract_body : ('a,'b) t -> head:'a -> ('a,'b) body * ('a,'b) frame * (('a * [`Input]) Sette.t * ('a * [`Output]) Sette.t) *)
val extract_body : ('a,'b) t -> head:'a -> ('a,'b) extraction

(** Extracts the body of a component indicated by its head. *)
val extract_body_only : ('a,'b) t -> head:'a -> ('a,'b) body

(* Subject to evolution and generalization. *)

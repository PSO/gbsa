open Wto_types
open Wto_open
open Wto_closed

(** Single entry graph *)
module type SE_GRAPH = sig
  type 'a t
  val root : 'a t -> 'a
  val succ : 'a t -> 'a -> 'a list
end

(** Partitions *)
module Part = struct
  type 'a t = 'a Fragment.t
  let empty = Fragment.empty
  let cons_elem = Fragment.cons_node
  let cons_sub =  Fragment.cons_frag
  let append = Fragment.append
  let print = Fragment.print
  let print_heads = Fragment.print_heads
  let to_wto = of_fragment
end

(** Ordering *)
module Ordering = struct
  type t = GT | EQ | LT
  let int a b = if a > b then GT else if a < b then LT else EQ
end

(** Node numbering *)
module Numbering = struct
  type t = Cut | Num of int
  let min a b = match (a,b) with
    | x, Cut
    | Cut, x -> x
    | Num na, Num nb -> Num (min na nb)
  let lt a b = match (a,b) with
    | Cut, Cut -> raise (Invalid_argument "Numbering: Comparing Cut with Cut")
    | _, Cut -> true
    | Cut, _ -> false
    | Num na, Num nb -> na < nb
  let eq a b = match (a,b) with
    | Cut, Cut -> raise (Invalid_argument "Numbering: Comparing Cut with Cut")
    | _, Cut
    | Cut, _ -> false
    | Num na, Num nb -> na = nb
  let order a b = match (a,b) with
    | Cut, Cut -> raise (Invalid_argument "Numbering: Comparing Cut with Cut")
    | Cut, _ -> Ordering.GT
    | _, Cut -> Ordering.LT
    | Num na, Num nb -> Ordering.int na nb
end

(** Depth First Numbering *)
module DFN = struct
  type 'a t = { nxt : int;
		map : ('a, Numbering.t) Mappe.t }
  let empty = { nxt = 0; map = Mappe.empty }
  let number dfn x =
    let n = Numbering.Num dfn.nxt in
    n, { nxt = dfn.nxt + 1;
         map = Mappe.add x n dfn.map }
  let get dfn x = try Some (Mappe.find x dfn.map)
    with Not_found -> None
  let cut dfn x = { dfn with map = Mappe.add x Numbering.Cut dfn.map }
  let forget dfn s = { dfn with map = Mappe.diffset dfn.map s }
end

(** Visit and exploration result *)
module VRes = struct
  type 'a t = { min : Numbering.t;   (** Smallest numbering met during the visit *)
		part : 'a Part.t;    (** Partition decided during the visit *)
		rem : 'a Sette.t;    (** Other nodes met during the visit *) }
  let empty = { min = Numbering.Cut; rem = Sette.empty; part = Part.empty }
  let visited num = { min = num; rem = Sette.empty; part = Part.empty }
  let append r1 r2 = { min = Numbering.min r1.min r2.min;
                       rem = Sette.union r1.rem r2.rem;
                       part = Part.append r1.part r2.part; }
end

(* Main algorithm for Weak Topological Ordering *)
let of_graph ~root ~succ =
  let open VRes in
  let rec visit vertex dfn =
    let num, dfn = DFN.number dfn vertex in
    let er, dfn = explore_succ vertex dfn in
    match Numbering.order num er.min with
    | Ordering.LT -> (* Not a loop *)
      let dfn = DFN.cut dfn vertex in
      { min = num; rem = Sette.empty;
        part = Part.cons_elem vertex er.part; }, dfn
    | Ordering.GT -> (* Inside a loop, not head *)
      { er with rem = Sette.add vertex er.rem }, dfn
    | Ordering.EQ -> (* Loop head *)
      let dfn = DFN.cut dfn vertex in
      let dfn = DFN.forget dfn er.rem in
      let subpart, dfn = component vertex dfn in
      { min = num; rem = Sette.empty;
        part = Part.cons_sub subpart er.part; }, dfn
  and component vertex dfn =
    let er, dfn = explore_succ vertex dfn in
    Part.cons_elem vertex er.part, dfn
  and explore_succ vertex dfn =
    List.fold_left (fun (er_acc,dfn) succ ->
      let er_succ, dfn = match DFN.get dfn succ with
        | Some num_succ -> VRes.visited num_succ, dfn
        | None -> visit succ dfn in
      VRes.append er_succ er_acc, dfn)
      (VRes.empty, dfn)
      (succ vertex) in
  let vr, _ = visit root DFN.empty
  in Part.to_wto vr.part

let of_graph_multi ~roots ~succ =
  let lift = List.map (fun n -> `Node n) in
  let succ = function
    | `Root -> lift roots
    | `Node x -> lift (succ x) in
  let w = of_graph `Root succ in
  let f = w.root in
  let c = List.tl (f.content) in
  let f = Fragment.of_list c in
  let f = Fragment.map
    (function
    | `Root -> assert false
    | `Node x -> x) f in
  of_fragment f

(** A [Hgraph] paired with a suitable [Wto]. *)

type ('a,'b) t = {
  graph : ('a,'b) Hgraph.t;
  wto : 'a Wto.t;
}


(** Running the analysis. *)

(** Controlling the widening. *)
module Widening : sig

  (** Outcome of a widening. *)
  type 'a result = {
    value : 'a;   (** Abstract value to continue with. *)
    halt : bool;  (** Should the component be iterated again? *)
  }

  (** Alternative way to build a result. *)
  val result : 'a -> halt:bool -> 'a result

  (** Widening controller. A controller lives until it claims it can halt. *)
  type 'a t = {
    widen : 'a -> 'a -> 'a result (** The widening operator. *)
  }

  (** In a finite height lattice, builds a widening controller from [le] and [join]. *)
  val basic : le:('a -> 'a -> bool) -> join:('a -> 'a -> 'a) -> 'a t
end

(** Abstract domain interface. *)
module type DOM = sig

  (** Type of the nodes of the hyper-graph. *)
  type node

  (** Type of the labels of the hyper-graph. *)
  type label

  (** Type of the abstract values. *)
  type abs

  (** Analysis context.
      User-defined information provided to every
      function having access to a [node] or a [label].
  *)
  type context

  (** Bottom element. *)
  val bot : context -> node -> abs

  (** Ordering on abstract values: less or equal.
      The function is such that [le a b] means [a <= b].
  *)
  val le : abs -> abs -> bool

  (** Least upper bound of two abstract values.
      The function is such that [a, b <= join a b].
  *)
  val join : abs -> abs -> abs

  (** Abstract difference.
      The function is such that [a <= join (diff a b) b].
      In doubt, [diff a _ = a] is a safe fallback function.
  *)
  val diff : abs -> abs -> abs

  (** Create a widening controller for a given wto component. *)
  val widen : context -> node Wto.component -> abs Postfp.Widening.t

  (** Should a narrowing be applied? *)
  val narrow : bool

  (** Abstract interpretation of a label. *)
  val interpret : context -> (node * abs) array -> label -> node -> abs

  (** Pretty-printer for nodes of the graph. *)
  val print_node : Format.formatter -> node -> unit

  (** Pretty-printer for labels of the graph. *)
  val print_label : Format.formatter -> label -> unit

  (** Pretty-printer for abstract values. *)
  val print_abs : Format.formatter -> abs -> unit
end

(** Making an analyser on a specific abstract domain. *)
module Make : functor (D : DOM) -> sig
  val analysis : (D.node,D.label) Wgraph.t -> D.context -> (D.node,D.abs) Mappe.t
end

open Wgraph_types

(** Building [Wgraph]. *)

let make g w =
  let check_hedge h =
    let o = Wto.ordering w in
    let dst = Hgraph.Hedge.dst h in
    let check_src n =
      if TotalOrder.le o dst n
	&& not (List.mem dst (Wto.node_heads w n))
      then raise (Invalid_argument "Edges of the graph contradict the wto") in
    List.iter check_src (Hgraph.Hedge.src h) in
  Hgraph.iter_hedges check_hedge g;
  if (Hgraph.node_count g <> Wto.cardinal w)
  then raise (Invalid_argument "Number of nodes mismatch between the graph and the wto");
  { graph = g; wto = w }

let of_graph ?entry g =
  let succ n = Sette.elements (Hgraph.succ g n) in
  let w = match entry with
    | Some r -> Wto.of_graph r succ
    | None -> match Sette.elements (Hgraph.roots g) with
      | [r] -> Wto.of_graph r succ
      | [] -> raise (Invalid_argument "graph with no roots")
      | l -> Wto.of_graph_multi l succ
  in
  make g w

let add_init wg lab dst = { wg with graph = Hgraph.add_init wg.graph lab dst }

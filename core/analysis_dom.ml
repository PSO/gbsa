open Analysis_widening

(** Analysis domain. *)

module type DOM = sig
  type node
  type label
  type abs
  type context
  val bot : context -> node -> abs
  val le : abs -> abs -> bool
  val join : abs -> abs -> abs
  val diff : abs -> abs -> abs
  val widen : context -> node Wto.component -> abs Postfp.Widening.t
  val narrow : bool
  val interpret : context -> (node * abs) array -> label -> node -> abs
  val print_node : Format.formatter -> node -> unit
  val print_label : Format.formatter -> label -> unit
  val print_abs : Format.formatter -> abs -> unit
end

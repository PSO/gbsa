(** Function unfolding *)

open Hgraph_types
open Hgraph_hedge
open Hgraph_inspect
open Hgraph_maker

let of_outgoing seed_l succ =
  let seed = List.fold_right Sette.add seed_l Sette.empty in
  let rec ro g s =
    if Sette.is_empty s
    then g
    else
      let n = Sette.choose s in
      let r = Sette.remove n s in
      let h,r = List.fold_left
	(fun (h,r) (l,n') ->
	  (Hedge.of_tuple ([n],l,n'))::h,
	  if not (contains g n') then Sette.add n' r else r)
	([],r) (succ n) in
      ro (add_hedge_list g h) r
  in
  ro empty seed

let of_incoming seed_l pred =
  let seed = List.fold_right Sette.add seed_l Sette.empty in
  let rec ri g s =
    if Sette.is_empty s
    then g
    else
      let n = Sette.choose s in
      let r = Sette.remove n s in
      let h,r = List.fold_left
	(fun (h,r) (ns',l) ->
	  (Hedge.of_tuple (ns',l,n))::h,
	  List.fold_left (fun r n' -> if not (contains g n') then Sette.add n' r else r) r ns')
	([],r) (pred n) in
      ri (add_hedge_list g h) r
  in
  ri empty seed

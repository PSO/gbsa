(** Total order implementation. *)

(** Ordering implementation. *)
module Ordering = struct
  type t = LT | EQ | GT
  let to_string = function
    | LT -> "LT"
    | EQ -> "EQ"
    | GT -> "GT"
  let print fmt oi = Format.pp_print_string fmt (to_string oi)
end

(** {3 Type} **)

type 'a t = {
  list : 'a array;
  indices : ('a, int) Mappe.t;
}

(** {3 Type-dependent operations} *)

let make a m =
  if Array.length a = 0
  then raise (Invalid_argument "empty order")
  else
    if Mappe.cardinal m <> Array.length a
    then raise (Invalid_argument "duplicated elements")
    else { list = a; indices = m }

let rank o x =
  try
    Mappe.find x o.indices
  with
    Not_found -> raise (Invalid_argument "not in the order")

let element o r =
  try
    Array.get o.list r
  with
    Invalid_argument _ -> raise (Invalid_argument (Print.sprintf "not a valid rank: %d" r))

let cardinal o = Array.length o.list

let compare o x y = (rank o x) - (rank o y)

let to_set o =
  let empty = PSette.empty (compare o) in
  Array.fold_right PSette.add o.list empty

let to_list o = Array.to_list o.list

let append o1 o2 =
  let a = Array.append o1.list o2.list in
  let n1 = Array.length o1.list in
  let ind2' = Mappe.map (fun i -> n1 + i) o2.indices in
  let ind = Mappe.addmap o1.indices ind2' in
  make a ind

let of_array a =
  let (n, ind) =
    Array.fold_left (fun (i, ind) x ->
      i + 1, Mappe.add x i ind)
      (0, Mappe.empty) a in
  make a ind

let fold f o acc =
  Array.fold_left (fun acc x -> f x acc) acc o.list

let foldi f o acc_u =
  fst (Array.fold_left (fun (acc_u, i) x -> f i x acc_u, i + 1) (acc_u, 0) o.list)

let fold_down f o =
  Array.fold_right f o.list

let foldi_down f o acc_u =
  fst (Array.fold_right (fun x (acc_u, i) -> f i x acc_u, i - 1) o.list (acc_u, cardinal o - 1))

let iter f o = Array.iter f o.list

let iteri f o = Array.iteri f o.list

let lookup f o =
  let rec rl i =
    if i >= Array.length o.list
    then None
    else match f i (Array.get o.list i) with
    | None -> rl (i + 1)
    | x -> x in
  rl 0

let lookdown f o =
  let rec rl i =
    if i < 0
    then None
    else match f i (Array.get o.list i) with
    | None -> rl (i - 1)
    | x -> x in
  rl (Array.length o.list - 1)

(** {3 Type-independent operations} *)

let of_list l = of_array (Array.of_list l)

let order o x y =
  let d = compare o x y in
  if d < 0 then Ordering.LT
  else if d = 0 then Ordering.EQ
  else Ordering.GT

let lt o x y = (compare o x y) < 0

let le o x y = (compare o x y) <= 0

let eq o x y = (compare o x y) = 0

let ge o x y = (compare o x y) >= 0

let gt o x y = (compare o x y) > 0

let bot o = element o 0

let top o = element o ((cardinal o) - 1)

let shift o d x =
  let r = (rank o x) + d in
  if r < 0 || r >= cardinal o then None else Some (element o r)

let next o = shift o 1

let prev o = shift o ~-1

let min o x y = if lt o y x then y else x

let max o x y = if gt o y x then y else x

let lub o = List.fold_left (max o) (bot o)

let glb o = List.fold_left (min o) (top o)

let rev o = of_list (List.rev (to_list o))

let iter_down f o = fold_down (fun x () -> f x) o ()

let iteri_down f o = foldi_down (fun i x () -> f i x) o ()

let find p o = match lookup (fun _ x -> if p x then Some x else None) o with
  | Some x -> x
  | None -> raise Not_found

let find_down p o = match lookdown (fun _ x -> if p x then Some x else None) o with
  | Some x -> x
  | None -> raise Not_found

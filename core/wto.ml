(**
   Weak Topological Ordering implementation.
*)

include Wto_types
include Wto_open
include Wto_closed
include Wto_analysis
include Wto_interpreter
include Wto_of_graph

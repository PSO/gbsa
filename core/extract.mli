(** Extraction of {!Wgraph} fragments. *)

module Relation : sig
  type 'a node
  val classify : 'a node -> [`Input | `Output | `Local]
  type ('a, 'b) t
  val graph : ('a, 'b) t -> ('a node, 'b) Wgraph.t
  val input : ('a, 'b) t -> 'a node Sette.t
  val output : ('a, 'b) t -> 'a node Sette.t
  val of_component : ('a, 'b) Wgraph.t -> head:'a -> ('a, 'b) t
end


(** Builders *)

open Hgraph_types
open Hgraph_hedge
open Hgraph_inspect

let empty = { inc_h = Mappe.empty; suc_n = Mappe.empty }

let of_hedge h =
  let dst = Sette.singleton h.dst in
  let dst_no_suc = Mappe.add h.dst Sette.empty Mappe.empty in
  let inc_h, suc_n = List.fold_left (fun (i,s) n ->
    Mappe.add n [] i, Mappe.add n dst s)
    (Mappe.empty, dst_no_suc)
    h.src in
  let inc_h = Mappe.add h.dst [h] inc_h in
  { inc_h = inc_h; suc_n = suc_n }

let add g1 g2 =
  let inc_h = Mappe.merge List.append g1.inc_h g2.inc_h in
  let suc_n = Mappe.merge Sette.union g1.suc_n g2.suc_n in
  { inc_h = inc_h; suc_n = suc_n }

let sum l = List.fold_left add empty l

let add_hedge g h = add g (of_hedge h)

let add_hyper g s l d = add_hedge g (Hedge.make s l d)

let add_tuple g (s,l,d) = add_hedge g (Hedge.make s l d)

let add_hedge_list g l = List.fold_left add_hedge g l

let add_tuple_list g l = List.fold_left add_tuple g l

let of_hedge_list l = add_hedge_list empty l

let of_tuple t = of_hedge (Hedge.of_tuple t)

let of_hyper s l d = of_tuple (s,l,d)

let of_tuple_list l = of_hedge_list (List.map Hedge.of_tuple l)

let add_hyper g sources label destination =
  add_hedge g { src = sources; lab = label; dst = destination }

let add_edge g src = add_hyper g [src]

let of_edge s l d = add_edge empty s l d

let add_init g = add_hyper g []

let mapi_labels f g =
  let fh h = { h with lab = f h } in
  let inc_h = Mappe.map (List.map fh) g.inc_h in
  { g with inc_h = inc_h }

let map_labels f = mapi_labels (fun h -> f h.lab)

let map_nodes f g =
  let inc_h = Mappe.fold (fun n inc ->
    Mappe.add (f n) (List.map (Hedge.map f) inc))
    g.inc_h
    Mappe.empty in
  let suc_n = Mappe.fold (fun n suc ->
    Mappe.add (f n) (Sette.fold (fun s ->
      Sette.add (f s)) suc Sette.empty))
    g.suc_n
    Mappe.empty in
  if Mappe.cardinal inc_h = Mappe.cardinal g.inc_h
  then { inc_h = inc_h; suc_n = suc_n }
  else failwith "map_nodes: distinct nodes map to a unique node"

let p_dst s h = Sette.mem h.dst s
let p_src_weak s h = List.exists (fun src -> Sette.mem src s) h.src
let p_src_strong s h = List.for_all (fun src -> Sette.mem src s) h.src

let filter_hedges p g =
  List.fold_left (fun g h -> if p h then add_hedge g h else g) empty (hedges g)

let filter_nodes p = filter_hedges (Hedge.all_nodes p)
let filter_destinations p = filter_hedges (Hedge.dst_sat p)

let transform_hedges f g =
  List.fold_left (fun acc h ->
    List.fold_left add_hedge acc (f h))
    empty (hedges g)

let project_nodes f g =
  List.fold_left (fun acc h -> add_hedge acc (Hedge.project_nodes f h)) empty (hedges g)

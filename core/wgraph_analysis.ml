open Wgraph_types
open Hgraph

module WA = Wto.Recursive

module Analysis = struct
  let postfp
      ?pp_node ?pp_label ?pp_value ?fmt
      wg
      ~bot ~join ~iterates ~interpret
      =
    let eval st n =
      List.fold_left (fun acc h ->
	let a = Array.of_list (List.map (fun p ->
	  let value = Mappe.find p st in
	  begin match pp_value, fmt with
	  | Some pp_value, Some fmt -> Format.fprintf fmt "Using input: %a@\n" pp_value value
	  | _ -> () end;
	  p,value) (Hedge.src h)) in
	begin match pp_label, fmt with
	| Some pp_label, Some fmt -> Format.fprintf fmt "Applying: %a@\n" pp_label (Hedge.lab h)
	| _ -> () end;
	let x = interpret a (Hedge.lab h) (Hedge.dst h) in
	join acc x) (bot n) (incoming wg.graph n) in
    WA.postfp
      ?pp_node ?pp_value ?fmt
      wg.wto
      ~bot ~iterates ~eval

end


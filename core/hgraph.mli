(**
   Hyper-graphs.

   Hyper-graphs are made of {e labelled hyper-edges} (in blue on the picture).
   An hyper-edge links a {e list of source} nodes to a {e unique destination} node.
   Nodes and hyper-edges labels are type parameters.

   An hyper-graph can be interpreted as an equation system
   whose variables map to nodes and functions map to egdes.
   When a node has several incoming edges, their results are implicitly combined by a {e join} operator.

   @image hgraph.tex.png
   @author pascal.sotin at irit.fr
*)

(**
   Hyper-edges.

   Representation of the hyper-edges and utilities.
*)
module Hedge : sig
    
  (** An hyper-edge carrying a label of type ['b] attached to nodes of type ['a]. *)
  type ('a,'b) t

  (** {3 Inspectors} *)

  (** The list of source nodes of the hyper-edge. *)
  val src : ('a,'b) t -> 'a list
    
  (** The destination node of the hyper-edge. *)
  val dst : ('a,'b) t -> 'a

  (** The label carried by the hyper-edge. *)
  val lab : ('a,'b) t -> 'b

  (** The number of sources nodes. *)
  val arity : ('a,'b) t -> int

  (** {3 Building hyper-edges} *)

  (** Builds an hyper-edge (currified version). *)
  val make : 'a list -> 'b -> 'a -> ('a,'b) t

  (** Builds an hyper-edge (with tuple version). *)
  val of_tuple : 'a list * 'b * 'a -> ('a,'b) t

  (** Map on nodes. *)
  val map : ('a -> 'b) -> ('a,'c) t -> ('b,'c) t

  (** Map on sources (index provided) and destination. *)
  val mapi : src:(int -> 'a -> 'b) -> dst:('a -> 'b) -> ('a,'c) t -> ('b,'c) t

  (** Map on sources (index provided), destination and label. *)
  val mapf : src:(int -> 'a -> 'b) -> dst:('a -> 'b) -> lab:('c -> 'd) -> ('a,'c) t -> ('b,'d) t

  (** {3 Printing} *)

  val print : 
    (Format.formatter -> 'a -> unit) ->
    (Format.formatter -> 'b -> unit) ->
    (Format.formatter -> ('a,'b) t -> unit)

  (** {3 Predicates} *)

  (** States if two hyper-edges are equal.
      If [eq_lab] is not provided, [(=)] is required for the labels. *)
  val equal : ?eq_label:('b -> 'b -> bool) -> ('a,'b) t -> ('a,'b) t -> bool

  (** {7 From node predicates} *)

  (** All the nodes (source and destination) satisfy the input predicate. *)
  val all_nodes : ('a -> bool) -> ('a,'b) t -> bool

  (** At least one node (source and destination) satisfies the input predicate. *)
  val any_node : ('a -> bool) -> ('a,'b) t -> bool

  (** All the source nodes satisfy the input predicate. *)
  val all_src : ('a -> bool) -> ('a,'b) t -> bool

  (** At least one source node satisfies the input predicate. *)
  val any_src : ('a -> bool) -> ('a,'b) t -> bool

  (** The destination node satisfies the input predicate. *)
  val dst_sat : ('a -> bool) -> ('a,'b) t -> bool

  (** All pair (source node, destination node) satisfies the input predicate. *)
  val all_src_dst : ('a -> 'a -> bool) -> ('a,'b) t -> bool

  (** At least one pair (source node, destination node) satisfies the input predicate. *)
  val any_src_dst : ('a -> 'a -> bool) -> ('a,'b) t -> bool

  (** {7 From sets of nodes} *)

  (** All the nodes (source and destination) belong to the set. *)
  val all_nodes_in : 'a Sette.t -> ('a,'b) t -> bool

  (** At least one node (source and destination) belongs to the set. *)
  val any_node_in : 'a Sette.t -> ('a,'b) t -> bool

  (** All the source nodes belong to the set. *)
  val all_src_in : 'a Sette.t -> ('a,'b) t -> bool

  (** At least one source node belongs to the set. *)
  val any_src_in : 'a Sette.t -> ('a,'b) t -> bool

  (** The destination node belongs to the set. *)
  val dst_in : 'a Sette.t -> ('a,'b) t -> bool

  (** {7 From arity} *)

  (** The arity is the provided one. *)
  val arity_eq : int -> ('a,'b) t -> bool

  (** The arity is greater or equal to the provided one. *)
  val arity_ge : int -> ('a,'b) t -> bool

  (** The arity is less or equal to the provided one. *)
  val arity_le : int -> ('a,'b) t -> bool

end

(** {2 Building graphs} *)

(**
   Hyper-graphs with nodes of type ['a] and labels of type ['b].

   Nodes are internally used as keys for sets or maps (comparison using [Pervasives.compare]).
   Labels are not used internally.
*)
type ('a,'b) t

(** {5 From scratch} *)

(** Empty graph. No nodes nor edges. *)
val empty : ('a,'b) t

(** Graph made of a one-to-one edge. *)
val of_edge : 'a -> 'b -> 'a -> ('a,'b) t

(** Graph made of a list of hyper-edge (described as tuples). *)
val of_tuple_list : ('a list * 'b * 'a) list -> ('a,'b) t

(** Graph made of a list of hyper-edge (already built). *)
val of_hedge_list : ('a,'b) Hedge.t list -> ('a,'b) t

(** Adds an a many-to-one edge to the graph. *)
val add_hyper : ('a,'b) t -> 'a list -> 'b -> 'a -> ('a,'b) t

(** Adds a one-to-one edge to the graph. *)
val add_edge : ('a,'b) t -> 'a -> 'b -> 'a -> ('a,'b) t

(** Adds a zero-to-one edge to the graph. *)
val add_init : ('a,'b) t -> 'b -> 'a -> ('a,'b) t

(** Adds an hyper-edge (described as a tuple) to the graph. *)
val add_tuple : ('a,'b) t -> ('a list * 'b * 'a) -> ('a,'b) t

(** Adds an hyper-edge (already built) to the graph. *)
val add_hedge : ('a,'b) t -> ('a,'b) Hedge.t -> ('a,'b) t

(** Add several hyper-edges (described as tuples) to the graph. *)
val add_tuple_list : ('a,'b) t -> ('a list * 'b * 'a) list -> ('a,'b) t

(** Adds several hyper-edges (already built) to the graph. *)
val add_hedge_list : ('a,'b) t -> ('a,'b) Hedge.t list -> ('a,'b) t

(** {5 From functions} *)

(** Builds a graph from a set of seeds and a successor function, going forward.
    The resulting graph only contains simple edges (arity 1). *)
val of_outgoing : 'a list -> ('a -> ('b * 'a) list) -> ('a,'b) t

(** Builds a graph from a set of seeds and a predecessor function, going backward. *)
val of_incoming : 'a list -> ('a -> ('a list * 'b) list) -> ('a,'b) t

(** {5 From graphs} *)

(** Adds two graphs. *)
val add : ('a,'b) t -> ('a,'b) t -> ('a,'b) t

(** Adds many graphs. *)
val sum : ('a,'b) t list -> ('a,'b) t

(** Renames the nodes of a graph, collapsing the structure when distinct nodes receive the same name. *)
val project_nodes : ('a -> 'b) -> ('a,'c) t -> ('b,'c) t

(** Builds a graph by transformation of the hyper-edges of another graph. *)
val transform_hedges : (('a,'b) Hedge.t -> ('c,'d) Hedge.t list) -> ('a,'b) t -> ('c,'d) t

(** {7 Isomorphisms} *)

(** Renames the nodes of a graph preserving the structure. This function will fail if distinct nodes receive the same name. *)
val map_nodes : ('a -> 'c) -> ('a,'b) t -> ('c,'b) t

(** Renames the labels of a graph. This preserves the structure. *)
val map_labels : ('b -> 'c) -> ('a,'b) t -> ('a,'c) t

(** Renames the labels of the hyper-edges of a graph. This preserves the structure. *)
val mapi_labels : (('a,'b) Hedge.t -> 'c) -> ('a,'b) t -> ('a,'c) t

(** {7 Subsets} *)

(** Filters the nodes of a graph, {e keeping} only those satisfying the predicate.
    Nodes are removed together with any hyper-edge reaching them or leaving them. *)
val filter_nodes : ('a -> bool) -> ('a,'b) t -> ('a,'b) t

(** Filters the hyper-edges of a graph, {e keeping} only those reaching a node satisfying the predicate.
    Their source nodes are also kept in the graph. *)
val filter_destinations : ('a -> bool) -> ('a,'b) t -> ('a,'b) t

(** Filters the hyper-edges of a graph, {e keeping} only those satisfying the predicate. *)
val filter_hedges : (('a,'b) Hedge.t -> bool) -> ('a,'b) t -> ('a,'b) t

(** {7 Compression} *)

(** Some nodes of a graph can be removed without altering its meaning
    by composing labels together. This process is called {e graph compression}.
*)

(** Compresses the graph packing labels in lists of labels. The protected nodes will not be removed.
    @image compress.tex.png
*)
val compress : ?protect:('a -> bool) -> ('a,'b) t -> ('a,'b list) t

(** Compresses the graph using user-specified combinators. The protected nodes will not be removed. *)
val compress_generic : ?protect:('a -> bool) -> lift:('b -> 'c) -> prepend:('b -> 'c -> 'c) -> ('a,'b) t -> ('a,'c) t

(** {2 Using graphs} *)

(** {7 Graph inspection} *)

(** Checks if a graph is empty. *)
val is_empty : ('a,'b) t -> bool

(** Checks if a node belongs to the graph. *)
val contains : ('a,'b) t -> 'a -> bool

(** Counts the number of nodes of the graph. *)
val node_count : ('a,'b) t -> int

(** Counts the number of hyper-edges of the graph. *)
val hedge_count : ('a,'b) t -> int

(** Returns the set nodes of the graph. *)
val nodes : ('a,'b) t -> 'a Sette.t

(** Returns the list of hyper-edges of the graph. *)
val hedges : ('a,'b) t -> ('a,'b) Hedge.t list

(** Returns the hyper-edges leaving a given node. *)
val outgoing : ('a,'b) t -> 'a -> ('a,'b) Hedge.t list

(** Returns the hyper-edges reaching a given node. *)
val incoming : ('a,'b) t -> 'a -> ('a,'b) Hedge.t list

(** Returns the successor nodes of a given node. *)
val succ : ('a,'b) t -> 'a -> 'a Sette.t

(** Returns the predecessor nodes of a given node. *)
val pred : ('a,'b) t -> 'a -> 'a Sette.t

(** Returns the nodes having no predecessor node. These nodes can still carry init edges. *)
val roots : ('a,'b) t -> 'a Sette.t

(** {7 Reachability} *)

(** Nodes of the graph reachable from a given set of node.
    A node is reachable if it is not excluded and
    either in the provided set
    or a successor of a non-frontier reachable node.
    The optional parameters [excluded]/[frontier] should
    return [true] on excluded/frontier nodes.

    In term of equations, the reachability reflects which variables
    are influenced by the provided variables.
*)
val reachable : ?excluded:('a -> bool) -> ?frontier:('a -> bool) -> ('a,'b) t -> 'a Sette.t -> 'a Sette.t

(** Nodes of the graph co-reachable from a given set of node.
    A node is co-reachable if it is not excluded and
    either in the provided set
    or a predecessor of a non-frontier co-reachable node.
    The optional parameters [excluded]/[frontier] should
    return [true] on excluded/frontier nodes.

    In term of equations, the co-reachability reflects which variables
    have an influence on the provided variables.
*)
val coreachable : ?excluded:('a -> bool) -> ?frontier:('a -> bool) -> ('a,'b) t -> 'a Sette.t -> 'a Sette.t

(* I also though of an parameter accepting an hyper-edge, and telling what
   to do with this edge (follow or cut). *)

(* There used to be notions of loose reachability / strict reachability / computability.
   Their definition and use were not obvious. *)

(* The [frontier] predicate could be extended to precise if the frontier nodes must be included of excluded. *)

(** {7 [fold] and [iter]} *)

val fold_nodes : ('a -> 'b -> 'b) -> ('a,'c) t -> 'b -> 'b

val foldi_nodes : (int -> 'a -> 'b -> 'b) -> ('a,'c) t -> 'b -> 'b

val fold_hedges : (('a,'b) Hedge.t -> 'c -> 'c) -> ('a,'b) t -> 'c -> 'c

val foldi_hedges : (int -> ('a,'b) Hedge.t -> 'c -> 'c) -> ('a,'b) t -> 'c -> 'c

val iter_nodes : ('a -> unit) -> ('a,'c) t -> unit

val iteri_nodes : (int -> 'a -> unit) -> ('a,'c) t -> unit

val iter_hedges : (('a,'b) Hedge.t -> unit) -> ('a,'b) t -> unit

val iteri_hedges : (int -> ('a,'b) Hedge.t -> unit) -> ('a,'b) t -> unit

(** {7 Printing} *)

(** Prints out the graph on a pretty-printer using the pretty pritting functions provided for nodes and labels.
    @include fig/docex_print.ml.html
*)
val print :
  (Format.formatter -> 'a -> unit) ->
  (Format.formatter -> 'b -> unit) ->
  (Format.formatter -> ('a,'b) t -> unit)

(** Outputs the graph in DOT format in the specified output channel (eg. [open_out "g.dot"]). The channel is closed afterwards unless [close] is set to [false].
    @image docex_dot.ml.dot.tex.png
*)
val dump_dot :
  (Format.formatter -> 'a -> unit) ->
  (Format.formatter -> 'b -> unit) ->
  ?close:bool ->
  ('a,'b) t ->
  out_channel ->
  unit

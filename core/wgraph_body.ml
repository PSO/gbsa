open Wgraph_types
open Wgraph_maker
open Wgraph_extract

type ('a,'b) body = ('a * [`Input | `Pre | `Body | `Local], 'b) t

type ('a,'b) frame = ('a * [`Input | `Pre | `Local | `Output],
		      [`Regular of 'b | `Bodies]) t

type ('a,'b) extraction =
  {
    pre_head : 'a * [`Pre];
    loc_head : 'a * [`Local];
    body : ('a,'b) body;
    frame : ('a,'b) frame;
    input : ('a * [`Input]) Sette.t;
    output : ('a * [`Output]) Sette.t;
  }

let extract_body (wg : ('a,'b) t) ~head =
  let i,cg,cw,o = extract_component_open wg ~head in

  (* Reachability in the component *)
  let loc_head = head, `Local in
  let is_head x = x = loc_head in
  let reach_i = Hgraph.reachable ~frontier:is_head cg i in
  let coreach_o = Hgraph.coreachable ~frontier:is_head cg o in
  let reach_h = Hgraph.reachable cg (Sette.singleton loc_head) in
  let coreach_h = Hgraph.coreachable cg (Sette.singleton loc_head) in

  (* Types of nodes *)
  let pre_node (n,t) =
    let pre_tag = function `Input -> `Input | `Local -> `Pre | _ -> assert false in
    (n, pre_tag t) in
  let pre_head = head, `Pre in

  (* Building the body *)
  let body = Sette.inter reach_h coreach_h in
  let body_g = Hgraph.transform_hedges (fun h ->
    let open Hgraph.Hedge in
    if dst_in body h && any_src_in body h
    then [mapi
	     ~src:(fun _ (n,t) ->
	       if n = head then (n,`Pre)
	       else if Sette.mem (n,t) body then (n,`Body)
	       else pre_node (n,t))
	     ~dst:(fun (n,t) ->
	       if n = head then (n,`Local)
	       else (n,`Body)) h]
    else []) cg in
  let body_w =
    let cwb = Wto.Component.map (fun (n,`Local) -> (n,`Body)) cw in
    let ocwb = List.tl (Wto.Component.content cwb) in
    let param = [] (* TODO: param is not computed! *) in
    let l = List.concat [param;[Wto.Node pre_head];ocwb;[Wto.Node loc_head]] in
    Wto.of_list l in
  let body_wg : ('a,'b) body = make body_g body_w in

  (* Building the frame *)  
  let pre = Sette.inter coreach_h reach_i in
  let pre_strict = Sette.remove loc_head pre in
  let coreach_o_strict = Sette.remove loc_head coreach_o in
  let loc_node (n,t) =  match t with
    | `Local -> (n,`Local)
    | `Input -> (n,`Input)
    | `Output -> (n,`Output) in
  let frame_g = Hgraph.transform_hedges (fun h ->
    let open Hgraph.Hedge in
    let lift ~src ~dst = mapf ~src:(fun _ -> src) ~dst ~lab:(fun l -> `Regular l) in
    let pre_edge = if any_src_in pre_strict h && dst_in pre h
      then begin
	assert (all_src_in pre_strict h);
	[lift ~src:pre_node ~dst:pre_node h] end
      else [] in
    let loc_edge = if any_src_in coreach_o h && dst_in coreach_o_strict h 
      then begin
	assert (all_src_in coreach_o h);
	[lift ~src:loc_node ~dst:loc_node h] end
      else [] in
    pre_edge @ loc_edge) cg in
  let frame_g = Hgraph.add_edge frame_g pre_head `Bodies loc_head in
  if false
  then Hgraph.dump_dot
    (fun fmt (_,t) -> Format.pp_print_string fmt (match t with
      `Input -> "I" | `Local -> "L" | `Pre -> "P" | `Output -> "O"))
    (fun fmt l -> Format.pp_print_string fmt (match l with
      `Bodies -> "Bodies" | `Regular _ -> "R"))
    frame_g
    (open_out "frame.dot"); (* debug code. Should disappear one day *)
  let frame_wg = of_graph frame_g in

  let i : ('a * [`Input]) Sette.t = i in
  let o : ('a * [`Output]) Sette.t = o in

  { pre_head = pre_head;
    loc_head = loc_head;
    body = body_wg;
    frame = frame_wg;
    input = i;
    output = o;
  }


let extract_body_only wg ~head =
  let i,cg,cw,o = extract_component_open wg ~head in
  let narrow_tag = function
    | `Local -> `Local
    | `Input -> `Input
    | `Output -> assert false in
  let narrow_node (n,t) = (n,narrow_tag t) in
  let head = narrow_node (Wto.Component.head cw) in
(*  let is_head x = x = head in
  let reach_i = Hgraph.reachable ~frontier:is_head cg i in
  let coreach_o = Hgraph.coreachable ~frontier:is_head cg o in *)
  let reach_h = Hgraph.reachable cg (Sette.singleton head) in
  let coreach_h = Hgraph.coreachable cg (Sette.singleton head) in
  let body = Sette.inter reach_h coreach_h in
  let body_g = Hgraph.transform_hedges (fun h ->
    let open Hgraph.Hedge in
    if dst_in body h && any_src_in body h
    then [mapi
	     ~src:(fun _ (n,t) ->
	       if (n,t) <> head && Sette.mem (n,t) body
	       then (n,`Body)
	       else narrow_node (n,t))
	     ~dst:(fun (n,_) -> (n,`Body)) h]
    else []) cg in
  let body_w =
    let cwb = Wto.Component.map (fun (n,`Local) -> (n,`Body)) cw in
    let ocwb = List.tl (Wto.Component.content cwb) in
(*    let param = Sette.fold (fun nt acc ->
      Wto.Node (narrow_node nt)::acc) (Hgraph.roots cg) [] in *)
    let l = List.concat [(*param;*)[Wto.Node head];ocwb;[Wto.Node (fst head,`Body)]] in
    Wto.of_list l in
  make body_g body_w
  

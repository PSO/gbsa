
module Relation = struct
  type 'a node = 'a * [`Input | `Local | `Output]
  type ('a, 'b) t = {
    input : 'a node Sette.t;
    output : 'a node Sette.t;
    graph : ('a node, 'b) Wgraph.t;
  }
  let graph r = r.graph
  let input r = r.input
  let output r = r.output
  let classify = snd

  let extract_component_open wg ~head =
    let graph = Wgraph.graph wg in
    let wto = Wgraph.wto wg in
    let open Hgraph in
    let l = Wto.node_components wto head in
    let c = match l with
      | c::_ when Wto.Component.head c = head -> c
      | _ -> raise (Invalid_argument "not a component head") in
    let in_component = Wto.mem_component wto c in
    let concerned = Hedge.any_node in_component in
    let sg = filter_hedges concerned graph in
    let map_src n = n, if in_component n then `Local else `Input in
    let map_dst n = n, if in_component n then `Local else `Output in
    let map_hedge h =
      [Hedge.make
	  (List.map map_src (Hedge.src h))
	(Hedge.lab h)
	  (map_dst (Hedge.dst h))] in
    let ng = transform_hedges map_hedge sg in
    let i,o = Sette.fold (fun x (i,o) -> match snd x with
      | `Input -> Sette.add (fst x,`Input) i,o
      | `Output -> i,Sette.add (fst x,`Output) o
      | `Local -> i,o) (nodes ng) (Sette.empty,Sette.empty) in
    let nw = Wto.Component.map (fun n -> n, `Local) c in
    i,ng,nw,o

  let of_component g ~head =
    let i,ng,nw,o = extract_component_open g ~head in
    let li = List.map (fun x -> Wto.Node x) (Sette.elements i) in
    let lo = List.map (fun x -> Wto.Node x) (Sette.elements o) in
    let nw = Wto.of_list (List.concat [li; [Wto.Component nw]; lo]) in
    { input = i;
      output = o;
      graph = Wgraph.make ng nw }

end

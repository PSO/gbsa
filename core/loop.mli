(** Separation of a specific loop of a {!Wgraph} into a body and a frame. *)

(** Graph representing the body of a loop.
    The body may contain nested loops.
    {ul
    {li The graph contains two versions of the head, before/after the iteration.}
    {li The graph contains the nodes of the body, both reachable and co-reachable from the heads.}
    {li The graph may contain parameters nodes, necessary for evaluating an iteration but not defined by the iteration. Only appears in presence of hyper-edges.}
    }
*)
module Body : sig

  module Node : sig
    type 'a t
    val classify : 'a t -> [`Param | `Before | `Body | `After]
    val origin : 'a t -> 'a
  end
  type ('a, 'b) t
  val graph : ('a, 'b) t -> ('a Node.t, 'b) Wgraph.t
  val head : [`Before | `After] -> ('a, 'b) t -> 'a Node.t
  val parameters : ('a, 'b) t -> 'a Node.t Sette.t
end

(** Graph representing the frame of a loop.
    {ul
    {li A unique special edge [`Bodies] represents 0 or more iterations of the loop body.}
    {li The graph contains variants of nodes of the loop before and/or after the iterations.
    If the loop is regular, this reduces to the head before and after.}
    }
*) 
module Frame : sig
  module Node : sig
    type 'a t
    val classify : 'a t -> [`Original | `Before | `After]
    val origin : 'a t -> 'a
  end
  module Label : sig
    type 'b t = [`Original of 'b | `Bodies]
  end
  type ('a, 'b) t
  val graph : ('a, 'b) t -> ('a Node.t, 'b Label.t) Wgraph.t
  val head : [`Before | `After] -> ('a, 'b) t -> 'a Node.t
end

type ('a, 'b) t
val body : ('a, 'b) t -> ('a, 'b) Body.t
val frame : ('a, 'b) t -> ('a, 'b) Frame.t
val extract : ('a, 'b) Wgraph.t -> head:'a -> ('a,'b) t

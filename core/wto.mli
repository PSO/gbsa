(**
   Weak Topological Ordering.

   A weak topological ordering (wto) on a graph is the combination of a total order on its nodes together with the definition of components. The order defines the notion of backward edges (destination "before" the source). A backward edge destination can only be the head of an enclosing component.

   @image wto.tex.png
   @author pascal.sotin at irit.fr
*)

(** Weak Topological Orderings. *)
type 'a t

(** {3 Entities composing a wto} *)

(** Components in a weak topological ordering. *)
type 'a component

(** Fragments of a weak topological ordering. *)
type 'a fragment

(** Elements in a weak topological ordering. *)
type 'a element =
| Node of 'a                (** A regular node *)
| Component of 'a component (** A component *)

(* I have in mind that exposing the elements is not that useful.
   Maybe it can be avoided. I tried not to expose "fragment" but its handy to have a module to work in. *)

(** Operating on components. *)
module Component : sig

  (** Retreives the head node of the component. *)
  val head : 'a component -> 'a

  (** Builds a component from a list of elements.
      This list must be non-empty and must start by a regular node. *)
  val of_list : 'a element list -> 'a component

  (** Builds a component from a wto fragment.
      This fragment must be non-empty and must start by a regular node. *)
  val of_fragment : 'a fragment -> 'a component

  (** Returns the content of the component.
      Inverse function for [of_list]. *)
  val content : 'a component -> 'a element list

  (** Rename the nodes. Renaming must not merge nodes. *)
  val map : ('a -> 'b) -> 'a component -> 'b component

  (** Nodes of the component. *)
  val nodes : 'a component -> 'a Sette.t
end

(** Operating on fragments. *)
module Fragment : sig
  (** Builds a wto fragment out of a node list *)
  val of_node_list : 'a list -> 'a fragment
    
  (** Builds a wto fragment out of a component. *)
  val of_component : 'a component -> 'a fragment

  (** Appends two fragments. *)
  val append : 'a fragment -> 'a fragment -> 'a fragment

  (** Rename the nodes. Renaming must not merge nodes. *)
  val map : ('a -> 'b) -> 'a fragment -> 'b fragment
end

(** {3 Building a wto}
    A wto is similar to a wto fragment, but also offers fast operations involving the order or the hierarchy. *)

(** Builds an element node. *)
val node : 'a -> 'a element

(** Builds a component element from an element list.
    The list must be non-empty and must not start by a component. *)
val component : 'a element list -> 'a element

(** Builds a wto from a list of elements.
    This list must be non-empty. *)
val of_list : 'a element list -> 'a t

val of_fragment : 'a fragment -> 'a t

val of_component : 'a component -> 'a t

(** Build a wto by exploring a single entry graph.
    The algorithm is due to Francois Bourdoncle, FMPA'93, fig. 4.
*)
val of_graph : root:'a -> succ:('a -> 'a list) -> 'a t

(** Build a wto by exploring a multiple entry graph. *)
val of_graph_multi : roots:('a list) -> succ:('a -> 'a list) -> 'a t

(** Returns the content of the wto.
    Inverse function for [of_list]. *)
val content : 'a t -> 'a element list

(** List of the heads of the wto (in increasing order). *)
val heads : 'a t -> 'a list

(* Tout ca fait vraiment de bric et de broc *)

(** List of the topmost components. *)
val topmost_components : 'a t -> 'a component list

(** Heads of the enclosing components of a given node.
    Deepest components first. *)
val node_heads : 'a t -> 'a -> 'a list

(** Enclosing components of a given node.
    Deepest components first. *)
val node_components : 'a t -> 'a -> 'a component list

(** Tells if a node belongs to the component. *)
val mem_component : 'a t -> 'a component -> 'a -> bool

(** Number of nodes in the wto. *)
val cardinal : 'a t -> int

(** {7 Underlying total ordering} *)

(** Underlying total ordering. *)
val ordering : 'a t -> 'a TotalOrder.t

(** Rank of a node in the total ordering. *)
val rank : 'a t -> 'a -> int

(** {7 Analysis} *)

(** Fixpoint computation based on the recursive iteration strategy.
   @image recursive.tex.png
*)
module Recursive : sig

  val postfp :
    ?pp_node:(Format.formatter -> 'a -> unit) ->
    ?pp_value:(Format.formatter -> 'b -> unit) ->
    ?fmt:Format.formatter ->
    'a t ->
    bot:('a -> 'b) ->
    iterates:('a component -> 'b Postfp.iterates) ->
    eval:(('a,'b) Mappe.t -> 'a -> 'b) ->
    ('a,'b) Mappe.t

end


open Wto_types
open Wto_open
open Wto_closed

(** {3 Analysis according to a WTO} *)

module State = struct
  type ('a,'b) t = 'a -> 'b
  let update st x v = fun y -> if y = x then v else st y
  let get st x = st x
end

let recursive
    (w : 'a t)
    (update : 'a -> ('a,'b) State.t -> 'b)
    (widen_controler : 'a Component.t -> 'b -> 'b -> 'b)
    (le : 'b -> 'b -> bool)
    (ini : ('a,'b) State.t)
    :
    ('a,'b) State.t
    =
  let rec element e st = match e with
    | Node n -> State.update st n (update n st)
    | Component c -> component c st
  and component c st =
    let widen = widen_controler c in
    let rec upward st =
      let vh = widen
	(State.get st c.head)
	(update c.head st) in
      let sh = State.update st c.head vh in
      let sc = fragment c.tail sh in
      if le (State.get sc c.head) vh then sc else upward sc in
    upward st
  and fragment f st =
    List.fold_left (fun acc e -> element e acc) st f.content
  in
  fragment w.root ini

module Recursive = struct

  module State = struct
    type ('a,'b) t = ('a,'b) Mappe.t
    let get st x = Mappe.find x st
    let to_mappe st = st
  end

  let pp_node_def w fmt n = Format.fprintf fmt "#%d" (rank w n)
  let pp_value_def fmt v = Format.fprintf fmt "@\n"

  let postfp
      ?pp_node ?pp_value ?fmt
      w
      ~bot ~iterates ~eval =

    (* tracing *)
    let pp_node = match pp_node with
      | None -> pp_node_def w
      | Some pp -> pp in
    let pp_value = match pp_value with
      | None -> pp_value_def
      | Some pp -> fun fmt v -> Format.fprintf fmt ":@\n @[<hov 0>%a@]@\n" pp v in
    let printf = match fmt with
      | None -> Format.ifprintf
      | Some _ -> Format.fprintf in
    let ofmt = fmt in
    let fmt = match fmt with
      | None -> Format.err_formatter (* dummy *)
      | Some fmt -> fmt in
    let pp_process_node_start fmt n = printf fmt
      "Processing node %a@\n" pp_node n in
    let pp_new_value fmt n v = printf fmt
      "New value for node %a%a" pp_node n pp_value v in
    let pp_enter fmt n = printf fmt
      "Entering the component headed by %a@\n" pp_node n in
    let pp_leave fmt n = printf fmt
      "Leaving the component headed by %a@\n" pp_node n in
    let pp_stable_check fmt n i = printf fmt
      "Checking the stability of the component headed by %a (iteration %d)@\n" pp_node n (i + 1) in
    let pp_unstable fmt n i = () in
    let pp_stable fmt n i = printf fmt
      "The component is stable@\n" in

    (* State management *)
    let all_bot = Mappe.mapofset (fun n -> bot n) (nodes w) in
    let update n v st =
      pp_new_value fmt n v;
      Mappe.add n v st in
    let get n st =
      Mappe.find n st in

    (* heart of the algorithm *)
    let rec process_fragment f st =
      List.fold_left (fun st e -> match e with
      | Node n -> process_node n st
      | Component c -> process_component c st) st f.content
    and process_node n st =
      pp_process_node_start fmt n;
      update n (eval st n) st
    and process_component c st =
      pp_enter fmt c.head;
      let iterates = iterates c in
      let st = process_node c.head st in
      let process_body = process_tail_head c in
      let rec postfp i st =
	let st' = process_body st in
	pp_stable_check fmt c.head i;
	match iterates ?fmt:ofmt i (get c.head st) (get c.head st') with
	| `Next v -> begin
	  pp_unstable fmt c.head i;
	  postfp (i + 1) (update c.head v st')
	end
	| `Stable -> begin
	  pp_stable fmt c.head i;
	  st'
	end
      in
      let st = postfp 0 st in
      pp_leave fmt c.head;
      st
    and process_tail_head c st =
      st |> process_fragment c.tail |> process_node c.head
    in
    process_fragment w.root all_bot
end

let recursive_postfp
    ~(bot : 'b)
    ~(postfp : ('b -> 'b) -> 'b -> 'b)
    ~(eval : 'a -> ('a,'b) Mappe.t -> 'b)
    (wto : 'a t)
    : ('a,'b) Mappe.t
    =
  let set_value n v st =
    Mappe.add n v st in
  let all_bot = Mappe.mapofset (fun _ -> bot) (nodes wto) in
  
  let postfp h process st =
    let cur_st = ref st in
    let _ = postfp (fun _ ->
      cur_st := process !cur_st;
      Mappe.find h !cur_st) (Mappe.find h st) in
    !cur_st in
      
  let rec process_fragment f st =
    List.fold_left (fun st e -> match e with
    | Node n -> process_node n st
    | Component c -> process_component c st) st f.content
  and process_node n st =
    set_value n (eval n st) st
  and process_component c st =
    st |> process_node c.head |> postfp c.head (process_tail_head c)
  and process_tail_head c st =
    st |> process_fragment c.tail |> process_node c.head in
  
  process_fragment wto.root all_bot

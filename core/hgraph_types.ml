(** Hgraph types. *)

(** An hyper-edge. *)
type ('a,'b) hedge = {
  src : 'a list;   (** Source nodes. *)
  lab : 'b;        (** Label. *)
  dst : 'a;        (** Destination node. *)
}

(** An hyper-graph. *)
type ('a,'b) hgraph = {
  inc_h : ('a, ('a,'b) hedge list) Mappe.t;
  (** Incoming hyper-edges. Invariant: the destination is equal to the key. *)
  suc_n : ('a,'a Sette.t) Mappe.t;
  (** Successor nodes. *)
}

type ('a,'b) t = ('a,'b) hgraph

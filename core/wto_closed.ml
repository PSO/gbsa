(**
   Final form of a weak topological ordering.
*)

open Wto_types
open Wto_open

(** {3 Main type} *)

type 'a t = 'a wto

let of_fragment f =
  let nodes,all = Fragment.fold_backward (fun (_,env) n (nodes,all) ->
    Mappe.add n env nodes, n :: all) f (Mappe.empty, []) in
  let ranks = TotalOrder.of_list all in
  assert (Mappe.cardinal nodes = TotalOrder.cardinal ranks);
  { root = f;
    nodes = nodes;
    ranks = ranks }
let of_list l = of_fragment (Fragment.of_list l)
let of_component c = of_fragment (Fragment.of_component c)

let make = of_list
let component l = Component (Component.of_list l)
let node n = Node n

let content w = Fragment.content w.root
let fold f w acc = Fragment.fold f w.root acc
let fold_component f w acc = Fragment.fold_components f w.root acc
let fold_elements_after fn fc w acc = Fragment.fold_elements_after fn fc w.root acc
let is_head w h = match Mappe.find h w.nodes with
  | c::_ when c.head = h -> true
  | _ -> false
let mem w n = Mappe.mem n w.nodes
let rank w n = TotalOrder.rank w.ranks n
let order w n1 n2 = (rank w n2) - (rank w n1)
let succ w n = TotalOrder.next w.ranks n
let nodes w = Mappe.maptoset w.nodes

(** {3 Camllib utility functions.} *)

let disjoint_add ?(msg="element is in the map") x v m =
  if Mappe.mem x m
  then raise (Invalid_argument msg)
  else Mappe.add x v m

let disjoint_union ?(msg="non disjoint maps") m1 m2 =
  Mappe.merge (fun _ _ -> raise (Invalid_argument msg)) m1 m2

(** {3 Builders } *)

let fragment_of_element_list (l : 'a element list) : 'a fragment =
  let subs = List.fold_right (fun x acc -> match x with
    | Node a -> acc
    | Component c -> c::acc) l [] in
  { content = l; subs = subs }

let element_list_of_list (l : 'a list) : 'a element list =
  List.map (fun x -> Node x) l

let fragment_of_list (l : 'a list) : 'a fragment =
  let el = element_list_of_list l in
  fragment_of_element_list el

let element_list_of_component (c : 'a component) : 'a element list =
  [Component c]

let fragment_of_component (c : 'a component) : 'a fragment =
  let el = element_list_of_component c in
  fragment_of_element_list el

let open_component (c : 'a component) : 'a element list =
  (Node c.head)::c.tail.content

let fragment_to_component (w : 'a fragment) : 'a component =
  match w.content with
  | (Node h)::t -> { head = h; tail = { w with content = t } }
  | (Component _)::_ -> raise (Invalid_argument "wto starting by a component")
  | [] -> raise (Invalid_argument "empty wto")

let component_of_list (l : 'a list) : 'a component =
  fragment_to_component (fragment_of_list l)

let component_of_element_list (l : 'a element list) : 'a component =
  let w = fragment_of_element_list l in
  fragment_to_component w

(*** As much as Ilist ***)

(* cons: expensive. NOEXPORT. *)
let cons (e: 'a element) (w: 'a fragment) =
  fragment_of_element_list (e::w.content)

(* atome: meere shorthand for of_element_list [Element x]. NOEXPORT. *)
let of_node (a : 'a) =
  fragment_of_element_list [Node a]

(* list: see of_element_list *)

(* of_list: see of_list *)

let to_list w =
  let rec fragment_to_list o =
    List.flatten (List.map element_to_list o.content)
  and element_to_list = function
    | Node x -> [x]
    | Component c -> component_to_list c
  and component_to_list c =
    c.head::fragment_to_list c.tail
  in
  fragment_to_list w.root

(* hd: useless without tl. NOEXPORT. *)
let hd w = match w.content with
  | x::_ -> x
  | _ -> raise (Invalid_argument "empty wto")

(* tl: expensive. NOEXPORT. *)
let tl w = match w.content with
  | _::r -> fragment_of_element_list r
  | _ -> raise (Invalid_argument "empty wto")

(* length *)
let length w = Mappe.cardinal w.nodes

let cardinal w = TotalOrder.cardinal w.ranks

(* depth *)
let rec depth w =
  List.fold_left max 0 (List.map (fun c -> 1 + depth c.tail) w.subs)

(* append: would be preferable to work on list before appending. *)
let append w1 w2 =
  fragment_of_element_list (List.append w1.content w2.content)

(* flatten: break the weakest topological ordering semantics. *)

(* rev: a wto is no a symetric structure. NOPROVIDE. *)

(* mem *)
let mem x w = Mappe.mem x w.nodes

(* exists: does not benefit from the map *)
let exists f w = Sette.exists f (Mappe.maptoset w.nodes)

let gen_map ~node ~comp ~update env w =
  fragment_of_element_list (List.map (function
  | Node a -> Node (node false env a)
  | Component c -> Component (comp (update env c) c)) w.content)

(* map *)
let rec map f w =
  let compf d c = { head = f true d c.head; tail = map f c.tail } in
  gen_map ~node:f ~comp:compf ~update:(fun x c -> c::x) [] w

let gen_fold ~node ~comp ~update env w acc =
  List.fold_left (fun acc e -> match e with
  | Node a -> node false env a acc
  | Component c -> comp (update env c) c acc) acc w.content

let rec fold f w acc =
  let compf d c acc =
    let hacc = f true d c.head acc in
    fold f c.tail hacc in
  gen_fold ~node:f ~comp:compf ~update:(fun x c -> c::x) [] w acc

(*** Beyond Ilist ***)

let components w =
  Fragment.fold_components_backward (fun c acc -> c::acc) w.root []
    
let first_node w = TotalOrder.bot w.ranks

let ordering w = w.ranks

let heads w =
  Fragment.fold_components_backward (fun c acc ->
    c.head::acc) w.root []

let topmost_components w = w.root.subs

let node_heads w n = List.map (fun c -> c.head) (Mappe.find n w.nodes)

let node_components w n = Mappe.find n w.nodes

let print_heads print_node fmt w = Fragment.print_heads print_node fmt w.root
let print print_node fmt w = Fragment.print print_node fmt w.root

let mem_component w c n = List.mem c (Mappe.find n w.nodes)

(*** Components packed with their wto ***)

type 'a comp_in_ctx = 'a wto * 'a component

let mk_comp_in_ctx c w = w,c

let comp_in_ctx w = Fragment.fold_components_backward (fun c acc ->
  (mk_comp_in_ctx c w)::acc) w.root []


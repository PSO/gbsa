(** Generator of Hyper-graphs *)
type node = int
let print_node fmt n = Format.fprintf fmt "n%d" n
  
type label = string
let print_label fmt l = Format.fprintf fmt "h_%s" l
  
type t = (node,label) Hgraph.t
let print = Hgraph.print print_node print_label
  
  (** Generates a line, starting at [ini] (default: [1]),
      adding [step] (default: [+1])
      and not producing nodes above [n] when [step] is positive,
      or below [n] when [step] is negative. *)
let line ?(ini=1) ?(step=1) n =
  let open Hgraph in
  let halt_on = match step with
    | 0 -> failwith "step should not be 0"
    | x when x > 0 -> (fun dst -> dst > n)
    | _ -> (fun dst -> dst < n) in 
  let rec rb src g =
    let dst = src + step in
    if halt_on dst then g
    else
      let g = add_edge g src ((string_of_int src)^"_"^(string_of_int dst)) dst in
      rb dst g in
  rb ini empty

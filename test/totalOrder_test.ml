open OUnit2
open TotalOrder

type 'a ref_ord = {
  rord : 'a list;
  rcmp : 'a -> 'a -> int;
  rle : 'a -> 'a -> bool;
  rlt : 'a -> 'a -> bool;
  req : 'a -> 'a -> bool;
  rgt : 'a -> 'a -> bool;
  rge : 'a -> 'a -> bool;
  pp : Format.formatter -> 'a -> unit;
}

(* ordered list *)
let l = [1;2;3;5]
let o = of_list l

let l2 = [10;20;5;25;15]
let o2 = of_list l2

let assert_equal_int e o = fun _ -> assert_equal ~printer:string_of_int e o

let assert_equal_data e o = fun _ -> assert_equal ~printer:string_of_int e o

let assert_equal_data_list e o = fun ctx ->
  let ppe = Format.pp_print_int in
  let ppl = Print.list ppe in
  let rec ra e o = match e, o with
    | [], [] -> ()
    | [], l -> assert_failure (Print.sprintf "obtained extra: %a" ppl l)
    | l, [] -> assert_failure (Print.sprintf "missing: %a" ppl l)
    | xe::re, xo::ro ->
      assert_equal_data xe xo ctx;
      ra re ro in
  ra e o

let assert_same_behavior2 ef_xy of_xy printer =
  List.concat (List.map (fun a1 ->
    List.map (fun a2 ->
      (Print.sprintf "%d %d" a1 a2)>::(fun _ -> assert_equal ~printer:printer (ef_xy a1 a2) (of_xy a1 a2)))
      l) l)

let sign x =
  if x = 0 then 0
  else if x < 0 then ~-1
  else 1

let ordering_of_list l x y =
  let upd i x z ox =
    if x = z then match ox with
    | None -> Some i
    | _ -> assert false
    else ox
  in
  match List.fold_left (fun (i,ox,oy) z ->
    i + 1, upd i x z ox, upd i y z oy) (0,None,None) l with
  | _, Some px, Some py ->
    if px = py then Ordering.EQ
    else if px < py then Ordering.LT
    else Ordering.GT
  | _ -> assert false

let assert_equal_int e o = fun _ -> assert_equal ~printer:string_of_int e o

let suite =

  let compare_test =
    "compare">:::assert_same_behavior2
      (fun x y -> sign (Pervasives.compare x y))
      (fun x y -> sign (compare o x y))
      string_of_int in

  let le_test =
    "le">:::assert_same_behavior2
      ( <= )
      (le o)
      string_of_bool in

  let lt_test =
    "lt">:::assert_same_behavior2
      ( < )
      (lt o)
      string_of_bool in

  let eq_test =
    "eq">:::assert_same_behavior2
      ( = )
      (eq o)
      string_of_bool in

  let gt_test =
    "gt">:::assert_same_behavior2
      ( > )
      (gt o)
      string_of_bool in

  let ge_test =
    "ge">:::assert_same_behavior2
      ( >= )
      (ge o)
      string_of_bool in

  let order_test =
    "order">:::assert_same_behavior2
      (fun x y -> ordering_of_list l x y)
      (order o)
      Ordering.to_string in

  let cardinal_test =
    "cardinal">:::
      ["l">::assert_equal_int (List.length l) (cardinal o);
       "l2">::assert_equal_int (List.length l2) (cardinal o2)] in

  let lub_test =
    "lub">:::
      ["{1,3}">::assert_equal_data 3 (lub o [1;3]);
       "{3,1}">::assert_equal_data 3 (lub o [3;1]);
       "{5}">::assert_equal_data 5 (lub o [5]);
       "{}">::assert_equal_data (List.hd l) (lub o [])] in

  let glb_test =
    "glb">:::
      ["{1,3}">::assert_equal_data 1 (glb o [1;3]);
       "{3,1}">::assert_equal_data 1 (glb o [3;1]);
       "{5}">::assert_equal_data 5 (glb o [5]);
       "{}">::assert_equal_data (List.hd (List.rev l)) (glb o [])] in

  let to_set_test =
    "to_set">:::
      ["l">::assert_equal_data_list l (PSette.elements (to_set o));
       "l2">::assert_equal_data_list l2 (PSette.elements (to_set o2))] in

  let fold_test =
    "fold">:::
      ["o">::fun c ->
	ignore
	  (fold (fun x r ->
	    assert_equal_data (element o r) x c;
	    r + 1) o 0)] in

  let foldi_test =
    "foldi">:::
      ["o">::fun c ->
	ignore
	  (foldi (fun i x r ->
	    assert_equal_int r i c;
	    assert_equal_data (element o r) x c;
	    r + 1) o 0)] in

  let fold_down_test =
    "fold_down">:::
      ["o">::fun c ->
	ignore
	  (fold_down (fun x r ->
	    assert_equal_data (element o r) x c;
	    r - 1) o ((cardinal o) - 1))] in

  let foldi_down_test =
    "foldi_down">:::
      ["o">::fun c ->
	ignore
	  (foldi_down (fun i x r ->
	    assert_equal_int r i c;
	    assert_equal_data (element o r) x c;
	    r - 1) o ((cardinal o) - 1))] in

  let iteri_test =
    "iteri">:::
      ["o">::fun c ->
	let r = ref 0 in
	iteri (fun i x ->
	  assert_equal_int !r i c;
	  assert_equal_data (element o !r) x c;
	  incr r) o] in

  let lookup_test =
    let p x = 2 <= x && x <= 4 in
    let f i x = if p x then Some (i,x) else None in
    let assert_find sf r v = fun c ->
	match sf f o with
	| None -> assert_failure "Should have found a value"
	| Some (i,x) ->
	  assert_equal_int r i c;
	  assert_equal_data v x c in
    "look">:::[
      "lookup">::assert_find lookup 1 2;
      "lookdown">::assert_find lookdown 2 3] in

  [compare_test; le_test; lt_test; eq_test; gt_test; ge_test;
   order_test; cardinal_test; lub_test; glb_test; to_set_test;
   fold_test; foldi_test; fold_down_test; foldi_down_test; iteri_test;
   lookup_test]

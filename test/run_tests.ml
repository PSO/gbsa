open OUnit2

let () =
  run_test_tt_main
    ("All tests">:::
	["Hgraph">:::Hgraph_test.suite;
	 "Hgraph_compress">:::Hgraph_compress_test.suite;
	 "TotalOrder">:::TotalOrder_test.suite;
	 "Wto">:::Wto_test.suite;
	 "Wgraph">:::Wgraph_test.suite])

open OUnit2
open Hgraph

let set_of_list l = List.fold_right Sette.add l Sette.empty
let set_to_string s = Print.sprintf "%a" (Sette.print Format.pp_print_int) s
let assert_equal_set e o = fun _ -> assert_equal ~printer:set_to_string ~cmp:Sette.equal e o

let assert_equal_int e o = fun _ -> assert_equal ~printer:string_of_int e o

let string_of_hedge_list l = Print.sprintf "%a" (Print.list (Hedge.print Format.pp_print_int Format.pp_print_char)) l

let assert_compatible_hedge_lists e o = fun _ ->
  match List.length e, List.length o with
  | le,lo when le <= 1 || lo <= 1 -> assert_equal ~printer:string_of_hedge_list e o
  | le,lo ->
    assert_equal ~printer:string_of_int le lo;
    assert_bool "hyper-hedges list mismatch"
      (List.for_all (fun h -> List.exists (Hedge.equal h) e) o)

let dump_ex = print Format.pp_print_int Format.pp_print_char

let graph_to_string g = Print.sprintf "%a" dump_ex g
let equal_graph g1 g2 =
  let h1 = hedges g1 in
  let h2 = hedges g2 in
  (List.length h1 = List.length h2)
  && List.for_all2 Hedge.equal h1 h2

let assert_equal_graph e o = fun _ -> assert_equal ~printer:graph_to_string ~cmp:equal_graph e o
(* Order of hedges assumed to be identical. *)

let doc_ex_hedges = List.map Hedge.of_tuple
  [[]   ,'i',1;
   [1]  ,'f',2;
   [1]  ,'g',3;
   [2;3],'h',4;
   [4]  ,'b',3]
let doc_ex = of_hedge_list doc_ex_hedges
let node_ex = set_of_list [1;2;3;4]

(* let _ = Format.printf "Documentation example = %a@\n" dump_ex doc_ex *)
  
let suite =

  (* Accessors. *)

  let is_empty_test =
    "is_empty">:::
      ["empty">::(fun _ -> assert_equal true (is_empty empty));
       "doc_ex">::(fun _ -> assert_equal false (is_empty doc_ex))] in
      
  let nodes_test =
    "nodes">:::
      ["empty">::(assert_equal_set Sette.empty (nodes empty));
       "doc_ex">::(assert_equal_set node_ex (nodes doc_ex))] in

  let node_count_test =
    "node_count">:::
      ["empty">::(assert_equal_int 0 (node_count empty));
       "doc_ex">::(assert_equal_int 4 (node_count doc_ex))] in

  let hedge_count_test =
    "hedge_count">:::
      ["empty">::(assert_equal_int 0 (hedge_count empty));
       "doc_ex">::(assert_equal_int 5 (hedge_count doc_ex))] in

  let hedges_test =
    "hedges">:::
      ["empty">::(fun _ -> assert_equal [] (hedges empty));
       "doc_ex">::(assert_compatible_hedge_lists doc_ex_hedges (hedges doc_ex))] in

  let succ_test =
    "succ">:::
      ["doc_ex">:::
	  ["n1">::(assert_equal_set (set_of_list [2;3]) (succ doc_ex 1));
	   "n2">::(assert_equal_set (set_of_list [4])   (succ doc_ex 2));
	   "n3">::(assert_equal_set (set_of_list [4])   (succ doc_ex 3));
	   "n4">::(assert_equal_set (set_of_list [3])   (succ doc_ex 4))]] in

  let pred_test =
    "pred">:::
      ["doc_ex">:::
	  ["n1">::(assert_equal_set (set_of_list [])    (pred doc_ex 1));
	   "n2">::(assert_equal_set (set_of_list [1])   (pred doc_ex 2));
	   "n3">::(assert_equal_set (set_of_list [1;4]) (pred doc_ex 3));
	   "n4">::(assert_equal_set (set_of_list [2;3]) (pred doc_ex 4))]] in

  let incoming_test =
    "incoming">:::
      ["doc_ex">:::
	  (List.map (fun i ->
	    ("n"^(string_of_int i))>::
	      (assert_compatible_hedge_lists
		 (List.filter (fun h -> Hedge.dst h = i) doc_ex_hedges)
		 (incoming doc_ex i)))
	     [1;2;3;4])] in

  let outgoing_test =
    "outgoing">:::
      ["doc_ex">:::
	  (List.map (fun i ->
	    ("n"^(string_of_int i))>::
	      (assert_compatible_hedge_lists
		 (List.filter (fun h -> List.mem i (Hedge.src h)) doc_ex_hedges)
		 (outgoing doc_ex i)))
	     [1;2;3;4])] in

(* Builders *)

  let map_labels_test =
    "map_labels">:::
      ["doc_ex">::
	  assert_equal_graph doc_ex (map_labels Char.chr (map_labels Char.code doc_ex))] in

  let map_nodes_test =
    "map_nodes">:::
      ["doc_ex">::
	  assert_equal_graph doc_ex (map_nodes Char.code (map_nodes Char.chr doc_ex))] in

  let mapi_labels_test =
    let wsrc src = fst (List.fold_left (fun (acc,rg) n -> acc + rg * n, rg + 1) (0,3) src) in
    let code h = Char.code (Hedge.lab h) + 2 * (Hedge.dst h) + wsrc (Hedge.src h) in
    let decode h = Char.chr ((Hedge.lab h) - 2 * (Hedge.dst h) - wsrc (Hedge.src h)) in
    "mapi_labels">:::
      ["doc_ex">::
	  assert_equal_graph doc_ex (mapi_labels decode (mapi_labels code doc_ex))] in

(*  let of_hedge_test =
    let h = Hedge.make [0] 'a' 1 in
    let g = of_hedge h in
    "of_hedge">:::
      ["nodes">::assert_equal_set (set_of_list [0;1]) (nodes g);
	"succ0">::assert_equal_set (set_of_list [1]) (succ g 0);
	"succ1">::assert_equal_set (set_of_list []) (succ g 1);
	"pred0">::assert_equal_set (set_of_list []) (pred g 0);
	"pred1">::assert_equal_set (set_of_list [0]) (pred g 1)] in *)
  
  let of_outgoing_test =
    let succ n = [Char.chr (n + Char.code 'a'), if n = 10 then 0 else n+1] in
    let g = of_outgoing [0] succ in
    let pred n =
      let p = if n = 0 then 10 else n-1 in
      [[p], Char.chr (p + Char.code 'a')] in
    let g' = of_incoming [0] pred in
    "of_outgoing">:::
      ["node_count">::(assert_equal_int 11 (node_count g));
       "as_incoming">::assert_equal_graph g g'] in

  let only_h keep = filter_hedges (fun h -> List.mem (Hedge.lab h) keep) doc_ex in
  let filter_hedges_test =
    "filter_hedges">:::
      ["only_h">::assert_equal_set (set_of_list [2;3;4]) (nodes (only_h ['h']));
       "only_i">::assert_equal_set (set_of_list [1]) (nodes (only_h ['i']));
       "only_fg">::assert_equal_set (set_of_list [1;2;3]) (nodes (only_h ['f';'g']))] in

  let only_n keep = filter_nodes (fun n -> List.mem n keep) doc_ex in
  let filter_nodes_test =
    "filter_hedges">:::
      ["only_1">::assert_equal_graph (only_h ['i']) (only_n [1]);
       "only_12">::assert_equal_graph (only_h ['i';'f']) (only_n [1;2]);
       "only_123">::assert_equal_graph (only_h ['i';'f';'g']) (only_n [1;2;3]);
       "only_34">::assert_equal_graph (only_h ['b']) (only_n [3;4])] in

  let only_d keep = filter_destinations (fun n -> List.mem n keep) doc_ex in
  let filter_destinations_test =
    "filter_destinations">:::
      ["only_1">::assert_equal_graph (only_h ['i']) (only_d [1]);
       "only_2">::assert_equal_graph (only_h ['f']) (only_d [2]);
       "only_3">::assert_equal_graph (only_h ['g';'b']) (only_d [3]);
       "only_14">::assert_equal_graph (only_h ['i';'h']) (only_d [1;4])] in

  let coreach from = coreachable doc_ex (set_of_list from) in
  let coreachable_test =
    "coreachable">:::
      ["from_1">::assert_equal_set (set_of_list [1]) (coreach [1]);
       "from_2">::assert_equal_set (set_of_list [1;2]) (coreach [2]);
       "from_3">::assert_equal_set (nodes doc_ex) (coreach [3]);
       "from_4">::assert_equal_set (nodes doc_ex) (coreach [4]);
       "from_all">::assert_equal_set (nodes doc_ex) (coreach [1;2;3;4])] in

  let reach from = reachable doc_ex (set_of_list from) in
  let reachable_test =
    "reachable">:::
      ["from_4">::assert_equal_set (set_of_list [3;4]) (reach [4]);
       "from_3">::assert_equal_set (set_of_list [3;4]) (reach [3]);
       "from_1">::assert_equal_set (nodes doc_ex) (reach [1]);
       "from_all">::assert_equal_set (nodes doc_ex) (reach [1;2;3;4])] in

  let transform_hedges_test =
    "transform_hedges">:::
      ["id">::assert_equal_graph doc_ex (transform_hedges (fun h -> [h]) doc_ex)] in

  let project_nodes_test =
    "project_nodes">:::
      ["id">::assert_equal_graph doc_ex (project_nodes (fun n -> n) doc_ex);
       "through_string">::assert_equal_graph doc_ex (project_nodes int_of_string (project_nodes string_of_int doc_ex))] in

(* Main suite *)

  [is_empty_test; nodes_test; node_count_test; hedge_count_test; hedges_test; succ_test; pred_test; incoming_test; outgoing_test;
   map_labels_test; map_nodes_test; mapi_labels_test; of_outgoing_test;
   filter_hedges_test; filter_nodes_test; filter_destinations_test;
   coreachable_test ; reachable_test;
   transform_hedges_test; project_nodes_test]

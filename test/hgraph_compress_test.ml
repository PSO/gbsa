open OUnit2
open Hgraph
open Hgraph_test

let assert_equal_list_graph e o = fun _ -> assert_equal ~cmp:equal_graph e o

let line = of_tuple_list
      [[1],'a',2;
       [2],'b',3;
       [3],'c',4;
       [4],'d',5]

let suite =

  let line_test =
    let e = of_edge 1 ['a';'b';'c';'d'] 5 in
    "line">::(assert_equal_list_graph e (Hgraph.compress line)) in

  let protect_test =
    let e = of_tuple_list
      [[1],['a';'b'],3;
       [3],['c';'d'],5] in
    let protect n = n == 3 in
    "protect">::(assert_equal_list_graph e (Hgraph.compress ~protect line)) in

(* Main suite *)

  [line_test;protect_test]

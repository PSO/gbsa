open OUnit2
open Wto

let assert_equal_node e o = fun _ -> assert_equal ~printer:string_of_int e o

let assert_equal_node_list e o = fun ctx ->
  let ppe = Format.pp_print_int in
  let ppl = Print.list ppe in
  let l_to_string l = Format.asprintf "%a" ppl l in
  assert_equal ~printer:l_to_string e o

let doc_ex =
  let mk_comp l = Component (Component.of_list l) in
  of_list [Node 1;
	   Node 2;
	   mk_comp [Node 3;
		    Node 4;
		    mk_comp [Node 5;
			     Node 6];
		    Node 7];
	   Node 8]
	   

let suite =

  let node_heads_test =
    let heads n = node_heads doc_ex n in
    "node_heads">:::
      ["n1">::(assert_equal_node_list [] (heads 1));
       "n2">::(assert_equal_node_list [] (heads 2));
       "n3">::(assert_equal_node_list [3] (heads 3));
       "n4">::(assert_equal_node_list [3] (heads 4));
       "n5">::(assert_equal_node_list [5;3] (heads 5));
       "n6">::(assert_equal_node_list [5;3] (heads 6));
       "n7">::(assert_equal_node_list [3] (heads 7));
       "n8">::(assert_equal_node_list [] (heads 8))] in

  let heads_test =
    "heads">::(assert_equal_node_list [3;5] (Wto.heads doc_ex)) in

  [node_heads_test;heads_test]

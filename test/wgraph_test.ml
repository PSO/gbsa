open OUnit2
open Wgraph

(* Data *)

let graph_w1 = Hgraph.of_tuple_list
  [[1],'e',2;
   [2],'l',2]
let head_w1 = 2
let wto_w1 = Wto.(of_list [node 1; component [node 2]])
let w1 = make graph_w1 wto_w1

let extract_w1 = {
  pre_head = 2,`Pre;
  loc_head = 2,`Local;
  body = make
    (Hgraph.of_tuple_list
       [[2,`Pre],'l',(2,`Local)])
    Wto.(of_list [node (2,`Pre); node (2,`Local)]);
  frame = make
    (Hgraph.of_tuple_list
       [[1,`Input],`Regular 'e',(2,`Pre);
	[2,`Pre],`Bodies,(2,`Local)])
    Wto.(of_list [node (1,`Input); node (2,`Pre); node (2,`Local)]);
  input = Sette.add (1,`Input) Sette.empty;
  output = Sette.empty
}

let graph_w2 = Hgraph.of_tuple_list
  [[1],'e',2;
   [1],'f',3;
   [2],'b',3;
   [2],'x',4;
   [3],'c',2;
   [3],'y',4]
let head_w2 = 2
let wto_w2 = Wto.(of_list [node 1; component [node 2; node 3]; node 4])
let w2 = make graph_w2 wto_w2

let extract_w2 = {
  pre_head = 2,`Pre;
  loc_head = 2,`Local;
  body = make
    (Hgraph.of_tuple_list
       [[2,`Pre],'b',(3,`Body);
	[3,`Body],'c',(2,`Local)])
    Wto.(of_list [node (2,`Pre); node (3,`Body); node (2,`Local)]);
  frame = make
    (Hgraph.of_tuple_list
       [[1,`Input],`Regular 'e',(2,`Pre);
	[1,`Input],`Regular 'f',(3,`Pre);
	[1,`Input],`Regular 'f',(3,`Local);
	[3,`Pre],`Regular 'c',(2,`Pre);
	[2,`Pre],`Bodies,(2,`Local);
	[2,`Local],`Regular 'b',(3,`Local);
	[2,`Local],`Regular 'x',(4,`Output);
	[3,`Local],`Regular 'y',(4,`Output)])
    Wto.(of_list [node (1,`Input); node (3,`Pre); node (2,`Pre);
		  node (2,`Local); node (3,`Local); node (4,`Output)]);
  input = Sette.add (1,`Input) Sette.empty;
  output = Sette.add (4,`Output) Sette.empty;
}

(* Asserts *)

let print_label = Format.pp_print_char

let print_frame_label fmt = function
  | `Bodies -> Format.pp_print_string fmt "=Bodies="
  | `Regular l -> print_label fmt l

let print_node fmt (i,t) = Format.fprintf fmt "%c%d"
  (match t with
  | `Pre -> 'P'
  | `Local -> 'L'
  | `Body -> 'B'
  | `Input -> 'I'
  | `Output -> 'O') i

let string_of_node n = Print.sprintf "%a" print_node n

let string_of_node_set s = Print.sprintf "%a" (Sette.print print_node) s

let string_of_body g = Print.sprintf "%a" (Hgraph.print print_node print_label) g

let string_of_frame g = Print.sprintf "%a" (Hgraph.print print_node print_frame_label) g
					     
let assert_equal_node e o ctx =
  assert_equal ~printer:string_of_node e o

let assert_equal_node_set e o ctx =
  assert_equal ~printer:string_of_node_set ~cmp:Sette.equal e o

let assert_equal_body e o ctx =
  assert_equal ~printer:string_of_body ~cmp:Hgraph_test.equal_graph (graph e) (graph o)

let assert_equal_frame e o ctx =
  assert_equal ~printer:string_of_frame ~cmp:Hgraph_test.equal_graph (graph e) (graph o)

let assert_equal_extract e o ctx =
  assert_equal_node e.pre_head o.pre_head ctx;
  assert_equal_node e.loc_head o.loc_head ctx;
  assert_equal_body e.body o.body ctx;
  assert_equal_frame e.frame o.frame ctx;
  assert_equal_node_set e.input o.input ctx;
  assert_equal_node_set e.output o.output ctx

(* Main suite *)

let suite =

  let extract_body_test =
    "extract_body">:::
      ["w1">::assert_equal_extract extract_w1 (extract_body ~head:head_w1 w1);
       "w2">::assert_equal_extract extract_w2 (extract_body ~head:head_w2 w2)] in
  
  [extract_body_test]

(** Return HTML code for the given text of a image. *)
let html_of_image t =
  List.fold_left (fun acc e ->
    match e with
    | Odoc_info.Raw s ->
      let file = "../fig/"^s in
      acc^"<img src=\""^file^"\"><br>"
    | _ -> acc^"@image EXPECTING A FILE BASENAME") "<br/>" t

(** Return HTML code for the given text of a image. *)
let html_of_include t =
  List.fold_left (fun acc e ->
    match e with
    | Odoc_info.Raw s ->
      let file = "../"^s in
      acc^"<iframe src=\""^file^"\"></iframe>"
    | _ -> acc^"@include EXPECTING A FILE NAME") "<br/>" t

module Generator (G : Odoc_html.Html_generator) =
struct
  class html =
    object(self)
      inherit G.html
      initializer
        tag_functions <- ("image", html_of_image) :: ("include", html_of_include) :: tag_functions
  end
end
let _ = Odoc_args.extend_html_generator (module Generator : Odoc_gen.Html_functor);;

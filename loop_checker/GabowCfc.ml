(** Coding Gabow 2000 CFC with OCaml in functional style *)

module type GRAPH = sig
  type 'a t
  val fold_vertices : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val fold_successors : ('a -> 'b -> 'b) -> ('a t * 'a) -> 'b -> 'b
end

module Path = struct
  type 'a record = {
    node : 'a;
    number : int;
    tail_numbers : ('a, int) Mappe.t;
  }
  type 'a t = {
    rev_path : 'a record list;
    rev_compress : int list;
  }
  let empty = { rev_path = []; rev_compress = [] }
  let push n p =
    match p.rev_path with
    | [] -> { rev_path = [{node = n; number = 0; tail_numbers = Mappe.empty}]; rev_compress = [0] }
    | h::t ->
      let x = h.number + 1 in
      { rev_path = {node = n; number = x; tail_numbers = Mappe.add h.node h.number h.tail_numbers }::p.rev_path;
	rev_compress = x::p.rev_compress }
  let contains n p =
    match p.rev_path with
    | [] -> false
    | h::_ -> n = h.node || Mappe.mem n h.tail_numbers
  let contract p n =
    let h = List.hd p.rev_path in
    if n = h.node then p
    else
      let x = Mappe.find n h.tail_numbers in
      let rec compress = function
	| y::l when y > x -> compress l
	| l -> l in
      { p with rev_compress = compress p.rev_compress }
  let isTopComponentRoot n p =
    let h = List.hd p.rev_path in
    n = h.node || List.hd p.rev_compress = Mappe.find n h.tail_numbers
  let popComponent p =
    let z = List.hd p.rev_compress in
    let rec pop comp = function
      | h::t when h.number > z -> pop (Sette.add h.node comp) t
      | l -> comp, l in
    let comp, rp = pop Sette.empty p.rev_path in
    { rev_path = rp; rev_compress = List.tl p.rev_compress },
    comp
end

module YPath = struct
  type 'a t = {
    rev_path : 'a list;
    length : int;
    content : ('a, int) Mappe.t;
    rev_compress : int list;
  }
  let empty = { rev_path = []; length = 0; content = Mappe.empty; rev_compress = [] }
  let push n p =
    { rev_path = n::p.rev_path;
      length = p.length + 1;
      content = Mappe.add n p.length p.content;
      rev_compress = p.length::p.rev_compress }
  let contains n p = Mappe.mem n p.content
  let contract p n =
    let x = Mappe.find n p.content in
    let rec compress = function
      | y::l when y > x -> compress l
      | l -> l in
    { p with rev_compress = compress p.rev_compress }
  let isTopComponentRoot n p = Mappe.find n p.content == List.hd p.rev_compress
  let popComponent p =
    let z = List.hd p.rev_compress in
    let rec pop (cont,comp) = function
      | n::l when Mappe.find n cont >= z -> pop (Mappe.remove n cont, Sette.add n comp) l
      | l -> (cont,comp,l) in
    let cont, comp, rp = pop (p.content,Sette.empty) p.rev_path in
    { rev_path = rp;
      length = z;
      content = cont;
      rev_compress = List.tl p.rev_compress;
    },
    comp
end

module CfcMapping = struct
  type 'a t = {
    cfc : 'a Sette.t list;
    mapping : ('a, 'a Sette.t) Mappe.t;
  }
  let empty = { cfc = []; mapping = Mappe.empty }
  let register cfc m =
    let mapping = Sette.fold (fun n acc -> Mappe.add n cfc acc) cfc m.mapping in
    { cfc = cfc::m.cfc; mapping = mapping }
  let binded n m =
    Mappe.mem n m.mapping
end

module Cfc = functor (G : GRAPH) -> struct

  let rec dfs (g,v) (path, map) =
    if CfcMapping.binded v map
    then (path, map)
    else
      let path = Path.push v path in
      let path, map = G.fold_successors (fun w (path, map) ->
	if Path.contains w path
	then Path.contract path w, map
	else dfs (g,w) (path, map))
	(g,v) (path,map) in
      if Path.isTopComponentRoot v path
      then
	let path, cfc = Path.popComponent path in
	let map = CfcMapping.register cfc map in
	path, map
      else
	path, map

  let strong g =
    G.fold_vertices (fun v map ->
	let path, map = dfs (g,v) (Path.empty, map) in
	assert (path = Path.empty);
	map)
      g CfcMapping.empty

end

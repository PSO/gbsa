(** Checking the validity of a set of loops. *)

type ('a,'b) loop =
  { head : 'a;
    body : ('a,'b) Hgraph.t }

type ('a,'b) path = ('a,'b) Hgraph.Hedge.t list

let validate
    (g : ('a,'b) Hgraph.t)
    (l : ('a,'b) loop Sette.t)
    :
    [ `Valid
    | `Invalid of ('a,'b) path ]
    =
  failwith "Todo"

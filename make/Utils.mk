#### Phony rules ####

# Displaying something with cat
.PHONY: %.cat
%.cat: %
	cat $*

# Displaying something with xdot
.PHONY: %.xdot
%.xdot: %
	xdot $*

# Displaying the content of a variable
.PHONY: %.echo
%.echo:
	@echo $*: $(origin $*)
	@echo "$(value $*)"

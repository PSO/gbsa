## Setup ##

# Makefile
doc_M := make/Doc.mk

# Project name
doc_NAME := "GBSA: Graph-Based Static Analysis"

# Figures
doc_FIG := postfp hgraph compress total_order wto recursive docex_dot.ml.dot

## Build ##

doc_G := _build/DocImg.cmxs

# Custom documentation generator (including images)
$(doc_G): DocImg.ml $(doc_M)
	ocamlbuild -tag annot -use-ocamlfind -cflags -I,+ocamldoc $(notdir $@)
	@touch $@

# Building standalone figures from tex
%.tex.png: %.tex $(build_M)
	cd $(dir $<); pdflatex $(notdir $<)
	convert -density 150x150 $*.pdf -quality 90 $@

doc/index.html: $(doc_G) $(core) $(core_MLI) $(doc_M) doc.intro
	@mkdir -p $(dir $@)
	ocamldoc.opt $(shell ocamlfind query -i-format $(core_DEP)) -I _build/core -t $(doc_NAME) -intro doc.intro -g $< -d $(dir $@) $(core_MLI)

.PHONY: doc
doc: doc/index.html $(doc_FIG:%=fig/%.tex.png) fig/docex_print.ml.html $(core_MLI)

%.ml.dot: %.ml $(core) $(core_ML) $(core_MLI) $(doc_M)
	$(core_BUILD) -I core $*.native
	_build/$*.native > $@

fig/%.dot.tex: fig/%.dot $(doc_M)
	dot2tex --crop $< -o $@

%.ml.html: %.ml $(core) $(core_ML) $(core_MLI) $(doc_M)
	$(core_BUILD) -I core $*.native
	_build/$*.native > $@

## Setup ##

# Makefile
core_M := make/Core.mk

# Library name
core_LIB := gbsa

# Sources directory
core := core

# Dependency
core_DEP := camllib

# Exposed modules
core_MOD := hgraph postfp totalOrder wto wgraph extract loop analysis

## Build ##

core_CMA := _build/$(core)/$(core_LIB).cma
core_CMXA := _build/$(core)/$(core_LIB).cmxa
core_META := _build/$(core)/META
core_ML := $(wildcard $(core)/*.ml)
core_MLI := $(core_MOD:%=$(core)/%.mli)
core_BUILD := ocamlbuild -r -tag annot -use-ocamlfind $(core_DEP:%=-pkgs %)

$(core_CMA): $(core) $(core_ML) $(core_MLI) $(core_M)
	$(core_BUILD) -log core_cma.log -tag debug $(core_MLI:%.mli=%.cmo)
	cat _build/core_cma.log \
	| perl -ne 'if ($$_ =~ /# Target: (.*?\.cmo).*/) { print "_build/$$1\n"; }' > $(core_CMA).lst
	cat $(core_CMA).lst | xargs ocamlc -a -o $@

$(core_CMXA): $(core) $(core_ML) $(core_MLI) $(core_M)
	$(core_BUILD) -log core_cmxa.log $(core_MLI:%.mli=%.cmx)
	cat _build/core_cmxa.log \
	| perl -ne 'if ($$_ =~ /# Target: (.*?\.cmx).*/) { print "_build/$$1\n"; }' > $(core_CMXA).lst
	cat $(core_CMXA).lst | xargs ocamlopt -a -o $@

$(core_META): $(core_M)
	@mkdir -p $(dir $@)
	echo 'name="$(core_LIB)"' > $@
	echo 'description="Graph-based Static Analyser"' >> $@
	echo 'version="0.61"' >> $@
	echo 'depends="$(core_DEP)"' >> $@
	echo 'archive(byte)="$(core_LIB).cma"' >> $@
	echo 'archive(native)="$(core_LIB).cmxa"' >> $@

.PHONY: lib
lib: $(core_CMA) $(core_CMXA)

.PHONY: install
install: lib $(core_META)
	cd _build/$(core); ocamlfind install $(core_LIB) *

.PHONY: uninstall
uninstall:
	ocamlfind remove $(core_LIB)

.PHONY: reinstall
reinstall: lib $(core_META)
	ocamlfind remove $(core_LIB)
	cd _build/$(core); ocamlfind install $(core_LIB) *
